﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        public enum TypeComand
        {
            Координаты = 0,
            ФРЧ = 1,
            ППРЧ = 2,
            Квитанция = 3
        }
        public void SendССDataRequest(byte receiver, TypeComand typeComand)
        {
            // Текстовое сообщение 
            int iAdrReceive = receiver;
            int iCodeCmd = 1;

            RequestDataSend.Init();

            RequestDataSend.bSign = (byte)typeComand;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public void SendССSync(byte receiver, DateTime time)
        {
            // Синхронизация
            int iAdrReceive = receiver;
            int iCodeCmd = 2;

            SynchronizeSend.Init();

            SynchronizeSend.bHour = Convert.ToByte(time.Hour);
            SynchronizeSend.bMin = Convert.ToByte(time.Minute);
            SynchronizeSend.bSec = Convert.ToByte(time.Second);

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public void SendCCTextMessage(byte receiver, string message)
        {
            // Текстовое сообщение
            int iAdrReceive = receiver;
            int iCodeCmd = 3;

            TextMessageSend.Init();
            TextMessageSend.strText = "";
            for (int i = 0; i < 150; i++)
                TextMessageSend.bText[i] = 0;

            TextMessageSend.strText = message;

            int iLenText = 0;
            iLenText = TextMessageSend.strText.Length;

            TextMessageSend.bCountPack = Convert.ToByte(Math.Ceiling(iLenText / 9.0));
            byte[] bSendText = Encoding.Default.GetBytes(TextMessageSend.strText);

            for (int i = 0; i < iLenText; i++)
                TextMessageSend.bText[i] = bSendText[i];


            for (int i = 0; i < iLenText; i++)
            {
                if ((TextMessageSend.bText[i] > 191) & (TextMessageSend.bText[i] <= 239))
                    TextMessageSend.bText[i] = Convert.ToByte((Convert.ToInt32(TextMessageSend.bText[i]) - 64));
                if ((TextMessageSend.bText[i] > 239) & (TextMessageSend.bText[i] <= 255))
                    TextMessageSend.bText[i] = Convert.ToByte((Convert.ToInt32(TextMessageSend.bText[i]) - 16));
            }

            TextMessageSend.bTempPack = 1;

            while (TextMessageSend.bTempPack <= TextMessageSend.bCountPack)
            {
                SendCommandToJammer(iCodeCmd, iAdrReceive);
                TextMessageSend.bTempPack++;
                OnWriteByte?.Invoke();
            }
        }

        public enum Mode
        {
            Подготовка = 0,
            Радиоразведка = 1,
            Радиоподавление = 2,
            ТренажРР = 3,
            ТренажРП = 4
        }
        public void SendCCWorkMode(byte receiver, Mode mode)
        {
            // Режим работы
            int iAdrReceive = receiver;
            int iCodeCmd = 4;

            RegimeWorkSend.Init();

            RegimeWorkSend.bRegime = (byte)mode;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public enum Action
        {
            Добавить = 0,
            Заменить = 1,
            Удалить = 2,
            ОчиститьСписок = 4
        }

        public void SendCCForbiddenRanges(byte receiver, Action action, byte rowNumber, double FreqMinkHz, double FreqMaxkHz)
        {
            // Запрещенные частоты
            int iAdrReceive = receiver;
            int iCodeCmd = 5;

            ForbidRangeFreqSend.Init();

            ForbidRangeFreqSend.bAction = (byte)action;

            ForbidRangeFreqSend.bTempRow = rowNumber;
            ForbidRangeFreqSend.iFreqBegin = (int)(Convert.ToDouble(FreqMinkHz) * 10);
            ForbidRangeFreqSend.iFreqEnd = (int)(Convert.ToDouble(FreqMaxkHz) * 10);

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCForbiddenRanges(byte receiver, Action action, byte rowNumber, int FreqMinkHzX10, int FreqMaxkHzX10)
        {
            // Запрещенные частоты
            int iAdrReceive = receiver;
            int iCodeCmd = 5;

            ForbidRangeFreqSend.Init();

            ForbidRangeFreqSend.bAction = (byte)action;

            ForbidRangeFreqSend.bTempRow = rowNumber;
            ForbidRangeFreqSend.iFreqBegin = FreqMinkHzX10;
            ForbidRangeFreqSend.iFreqEnd = FreqMaxkHzX10;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public void SendCCSectorsAndRangesRI(byte receiver, Action action, byte rowNumber, double FreqMinkHz, double FreqMaxkHz, ushort AngleMin, ushort AngleMax)
        {
            // Сектора и диапазоны РР
            int iAdrReceive = receiver;
            int iCodeCmd = 6;

            ReconSectorRangeSend.Init();

            ReconSectorRangeSend.bAction = (byte)action;

            ReconSectorRangeSend.bTempRow = rowNumber;
            ReconSectorRangeSend.iFreqBegin = (int)(FreqMinkHz * 10);
            ReconSectorRangeSend.iFreqEnd = (int)(FreqMaxkHz * 10);
            ReconSectorRangeSend.wAngleBegin = AngleMin;
            ReconSectorRangeSend.wAngleEnd = AngleMax;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCSectorsAndRangesRI(byte receiver, Action action, byte rowNumber, int FreqMinkHzX10, int FreqMaxkHzX10, ushort AngleMin, ushort AngleMax)
        {
            // Сектора и диапазоны РР
            int iAdrReceive = receiver;
            int iCodeCmd = 6;

            ReconSectorRangeSend.Init();

            ReconSectorRangeSend.bAction = (byte)action;

            ReconSectorRangeSend.bTempRow = rowNumber;
            ReconSectorRangeSend.iFreqBegin = FreqMinkHzX10;
            ReconSectorRangeSend.iFreqEnd = FreqMaxkHzX10;
            ReconSectorRangeSend.wAngleBegin = AngleMin;
            ReconSectorRangeSend.wAngleEnd = AngleMax;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public void SendCCSectorsAndRangesRS(byte receiver, Action action, byte rowNumber, byte prior, double FreqMinkHz, double FreqMaxkHz, ushort AngleMin, ushort AngleMax)
        {
            // Сектора и диапазоны РП
            int iAdrReceive = receiver;
            int iCodeCmd = 7;

            SupressSectorRangeSend.Init();
         
            SupressSectorRangeSend.bAction = (byte)action;

            SupressSectorRangeSend.bTempRow = rowNumber;
            SupressSectorRangeSend.bPrior = prior;
            SupressSectorRangeSend.iFreqBegin = (int)(FreqMinkHz * 10);
            SupressSectorRangeSend.iFreqEnd = (int)(FreqMaxkHz * 10);
            SupressSectorRangeSend.wAngleBegin = AngleMin;
            SupressSectorRangeSend.wAngleEnd = AngleMax;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCSectorsAndRangesRS(byte receiver, Action action, byte rowNumber, byte prior, int FreqMinkHzX10, int FreqMaxkHzX10, ushort AngleMin, ushort AngleMax)
        {
            // Сектора и диапазоны РП
            int iAdrReceive = receiver;
            int iCodeCmd = 7;

            SupressSectorRangeSend.Init();

            SupressSectorRangeSend.bAction = (byte)action;

            SupressSectorRangeSend.bTempRow = rowNumber;
            SupressSectorRangeSend.bPrior = prior;
            SupressSectorRangeSend.iFreqBegin = FreqMinkHzX10;
            SupressSectorRangeSend.iFreqEnd = FreqMaxkHzX10;
            SupressSectorRangeSend.wAngleBegin = AngleMin;
            SupressSectorRangeSend.wAngleEnd = AngleMax;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

        public enum TypeRS
        {
            ЧМ = 0,
            ЧМ2 = 1,
            ЧМ4 = 2,
            ЧМШ = 3,
            ФМн = 4,
            ЗШ = 5
        }

        public enum Deviation1
        {
            dev1_75 = 0,
            dev3_5 = 1,
            dev5_0 = 2,
            dev10 = 3,
            dev25 = 4,
            dev40 = 5,
            dev100 = 6,
            dev250 = 7,
            dev1000 = 8,
            dev2000 = 9,
            dev5000 = 10,
            dev8000 = 11,
            dev10000 = 12
        }

        public enum Deviation2
        {
            dev2 = 0,
            dev4 = 1,
            dev6 = 2,
            dev8 = 3,
            dev10 = 4,
            dev12 = 5,
            dev14 = 6,
            dev16 = 7,
            dev18 = 8,
            dev20 = 9,
            dev22 = 10,
            dev24 = 11,
        }

        public enum Deviation3
        {
            dev1_75 = 0,
            dev3_5 = 1,
            dev5_0 = 2,
            dev10 = 3,
            dev25 = 4,
            dev40 = 5,
            dev100 = 6,
            dev250 = 7,
            dev1000 = 8,
        }

        public enum Manipulation1
        {
            mani62_5 = 0,
            mani125 = 1,
            mani250 = 2,
            mani500 = 3,
            mani1000 = 4,
            mani2000 = 5,
            mani4000 = 6,
            mani8000 = 7
        }

        public enum Manipulation2
        {
            mani0_2 = 0,
            mani0_5 = 1,
            mani1 = 2,
            mani5 = 3,
            mani10 = 4,
            mani20 = 5,
            mani40 = 6,
            mani80 = 7,
            mani400 = 8
        }

        public void SendCCFRSonRS(byte receiver, Action action, byte rowNumber, byte prior, double FreqkHz, int codeHind, 
            TypeRS frsonRS, Deviation1 deviation1, Deviation2 deviation2, Deviation3 deviation3, Manipulation1 manipulation1, Manipulation2 manipulation2)
        {
            // Назначение на РП ИРИ ФЧ
            int iAdrReceive = receiver;
            int iCodeCmd = 8;

            SupressFWSSend.Init();

            SupressFWSSend.bAction = (byte)action;

            SupressFWSSend.bTempRow = rowNumber;
            SupressFWSSend.bPrior = prior;
            SupressFWSSend.iFreq = (int)(FreqkHz * 10);
            SupressFWSSend.iCodeHind = codeHind;

            // считать параметры помехи
            switch (frsonRS)
            {
                // ЧМ
                case TypeRS.ЧМ:
                    SupressFWSSend.bCodeModulation = 0x01;
                    SupressFWSSend.bCodeDeviation = (byte)deviation1;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ЧМ-2
                case TypeRS.ЧМ2:
                    SupressFWSSend.bCodeModulation = 0x02;
                    SupressFWSSend.bCodeDeviation = (byte)deviation2;
                    SupressFWSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМ-4
                case TypeRS.ЧМ4:
                    SupressFWSSend.bCodeModulation = 0x03;
                    SupressFWSSend.bCodeDeviation = (byte)deviation2;
                    SupressFWSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМШ
                case TypeRS.ЧМШ:
                    SupressFWSSend.bCodeModulation = 0x06;
                    SupressFWSSend.bCodeDeviation = (byte)deviation3;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ФМн
                case TypeRS.ФМн:
                    SupressFWSSend.bCodeModulation = 0x07;
                    // в байт девиации значение манипуляции
                    SupressFWSSend.bCodeDeviation = (byte)manipulation2;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ЗШ
                case TypeRS.ЗШ:
                    SupressFWSSend.bCodeModulation = 0x10;
                    SupressFWSSend.bCodeDeviation = 0;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                default:
                    SupressFWSSend.bCodeModulation = 0x06;
                    SupressFWSSend.bCodeDeviation = 0;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;
            }

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCFRSonRS(byte receiver, Action action, byte rowNumber, byte prior, int FreqkHzX10, int codeHind,
          TypeRS frsonRS, Deviation1 deviation1, Deviation2 deviation2, Deviation3 deviation3, Manipulation1 manipulation1, Manipulation2 manipulation2)
        {
            // Назначение на РП ИРИ ФЧ
            int iAdrReceive = receiver;
            int iCodeCmd = 8;

            SupressFWSSend.Init();

            SupressFWSSend.bAction = (byte)action;

            SupressFWSSend.bTempRow = rowNumber;
            SupressFWSSend.bPrior = prior;
            SupressFWSSend.iFreq = FreqkHzX10;
            SupressFWSSend.iCodeHind = codeHind;

            // считать параметры помехи
            switch (frsonRS)
            {
                // ЧМ
                case TypeRS.ЧМ:
                    SupressFWSSend.bCodeModulation = 0x01;
                    SupressFWSSend.bCodeDeviation = (byte)deviation1;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ЧМ-2
                case TypeRS.ЧМ2:
                    SupressFWSSend.bCodeModulation = 0x02;
                    SupressFWSSend.bCodeDeviation = (byte)deviation2;
                    SupressFWSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМ-4
                case TypeRS.ЧМ4:
                    SupressFWSSend.bCodeModulation = 0x03;
                    SupressFWSSend.bCodeDeviation = (byte)deviation2;
                    SupressFWSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМШ
                case TypeRS.ЧМШ:
                    SupressFWSSend.bCodeModulation = 0x06;
                    SupressFWSSend.bCodeDeviation = (byte)deviation3;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ФМн
                case TypeRS.ФМн:
                    SupressFWSSend.bCodeModulation = 0x07;
                    // в байт девиации значение манипуляции
                    SupressFWSSend.bCodeDeviation = (byte)manipulation2;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                // ЗШ
                case TypeRS.ЗШ:
                    SupressFWSSend.bCodeModulation = 0x10;
                    SupressFWSSend.bCodeDeviation = 0;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;

                default:
                    SupressFWSSend.bCodeModulation = 0x06;
                    SupressFWSSend.bCodeDeviation = 0;
                    SupressFWSSend.bCodeManipulation = 0;
                    break;
            }

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCFRSonRS(byte receiver, TSupressFWS supressFWSSend)
        {
            // Назначение на РП ИРИ ФЧ
            int iAdrReceive = receiver;
            int iCodeCmd = 8;

            SupressFWSSend = supressFWSSend;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }


        public void SendCCFHSSonRS(byte receiver, Action action, byte rowNumber, 
            double StepFHSSkHz, byte DuratFHSSms, double FreqMinkHz, double FreqMaxkHz,
             TypeRS frsonRS,
             Deviation1 deviation1, Deviation2 deviation2, Deviation3 deviation3, 
             Manipulation1 manipulation1, Manipulation2 manipulation2)
        {
            // Назначение на РП ИРИ ППРЧ
            int iAdrReceive = receiver;
            int iCodeCmd = 9;

            SupressFHSSSend.Init();

            SupressFHSSSend.bAction = (byte) action;

            SupressFHSSSend.bTempRow = rowNumber;
            SupressFHSSSend.bCodeStep = ConvertFromFHSSStepToStepCode(StepFHSSkHz);

            SupressFHSSSend.bGroupDuration = 0;
            SupressFHSSSend.bDuration = DuratFHSSms;

            SupressFHSSSend.iFreqBegin = (int)(FreqMinkHz * 10);
            SupressFHSSSend.iFreqEnd = (int)(FreqMaxkHz * 10);

            // считать параметры помехи
            switch (frsonRS)
            {
                // ЧМ
                case TypeRS.ЧМ:
                    SupressFHSSSend.bCodeModulation = 0x01;
                    SupressFHSSSend.bCodeDeviation = (byte)deviation1;
                    SupressFHSSSend.bCodeManipulation = 0;
                    break;

                // ЧМ-2
                case TypeRS.ЧМ2:
                    SupressFHSSSend.bCodeModulation = 0x02;
                    SupressFHSSSend.bCodeDeviation = (byte)deviation2;
                    SupressFHSSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМ-4
                case TypeRS.ЧМ4:
                    SupressFHSSSend.bCodeModulation = 0x03;
                    SupressFHSSSend.bCodeDeviation = (byte)deviation2;
                    SupressFHSSSend.bCodeManipulation = (byte)manipulation1;
                    break;

                // ЧМШ
                case TypeRS.ЧМШ:
                    SupressFHSSSend.bCodeModulation = 0x06;
                    SupressFHSSSend.bCodeDeviation = (byte)deviation3;
                    SupressFHSSSend.bCodeManipulation = 0;
                    break;

                // ФМн
                case TypeRS.ФМн:
                    SupressFHSSSend.bCodeModulation = 0x07;
                    // в байт девиации значение манипуляции
                    SupressFHSSSend.bCodeDeviation = (byte)manipulation2;
                    SupressFHSSSend.bCodeManipulation = 0;
                    break;

                // ЗШ
                case TypeRS.ЗШ:
                    SupressFHSSSend.bCodeModulation = 0x10;
                    SupressFHSSSend.bCodeDeviation = 0;
                    SupressFHSSSend.bCodeManipulation = 0;
                    break;

                default:
                    SupressFHSSSend.bCodeModulation = 0x06;
                    SupressFHSSSend.bCodeDeviation = 0;
                    SupressFHSSSend.bCodeManipulation = 0;
                    break;
            }

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCFHSSonRS(byte receiver, TSupressFHSS supressFHSSSend)
        {
            // Назначение на РП ИРИ ППРЧ
            int iAdrReceive = receiver;
            int iCodeCmd = 9;

            SupressFHSSSend = supressFHSSSend;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }


        public enum TypeReqBearing
        {
            Квазиодновременное = 0,
            Исполнительное =1
        }
        public void SendCCExOrQuasiBearingRequest(byte receiver, TypeReqBearing typeReqBearing, double FreqkHz, byte time)
        {
            // Запрос на ИП или КвП
            int iAdrReceive = receiver;
            int iCodeCmd = 10;

            RequestBearingSend.Init();

            RequestBearingSend.bSign = (byte)typeReqBearing;
            RequestBearingSend.iFreq = (int)(FreqkHz * 10);
            RequestBearingSend.bTime = time;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }
        public void SendCCExOrQuasiBearingRequest(byte receiver, TypeReqBearing typeReqBearing, int FreqkHzX10, byte time)
        {
            // Запрос на ИП или КвП
            int iAdrReceive = receiver;
            int iCodeCmd = 10;

            RequestBearingSend.Init();

            RequestBearingSend.bSign = (byte)typeReqBearing;
            RequestBearingSend.iFreq = FreqkHzX10;
            RequestBearingSend.bTime = time;

            SendCommandToJammer(iCodeCmd, iAdrReceive);
            OnWriteByte?.Invoke();
        }

    }
}
