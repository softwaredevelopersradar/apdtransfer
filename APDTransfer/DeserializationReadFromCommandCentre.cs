﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        private int ReadCommandFromAKKP(int iNumCmd, byte[] bBufRead)
        {
            int iRes = -1;

            for (int i = 0; i < 12; i++)
                bBufSend[i] = 0;

            byte[] bTemp = new byte[1];
            byte[] bByteBit = new byte[1];

            byte[] bByte2Bit = new byte[2];

            byte[] result;

            string strTimeCodeAdr = "";
            string strTypeCmd = "";
            string strInform = "";

            iAdrReceive = 0;


            switch (iNumCmd)
            {

                //--------------------------------
                case REQUEST_DATA:

                    RequestDataRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак запроса
                    RequestDataRead.bSign = bBufRead[2];                            //* 2 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Запрос данных\n";

                    if (RequestDataRead.bSign == 0)
                        strInform = "Координаты (код " + Convert.ToString(RequestDataRead.bSign) + ")\n";
                    if (RequestDataRead.bSign == 1)
                        strInform = "ИРИ ФЧ (код " + Convert.ToString(RequestDataRead.bSign) + ")\n";
                    if (RequestDataRead.bSign == 2)
                        strInform = "ИРИ ППРЧ (код " + Convert.ToString(RequestDataRead.bSign) + ")\n";
                    if (RequestDataRead.bSign == 3)
                        strInform = "Квитанция о подавляемых ИРИ (код " + Convert.ToString(RequestDataRead.bSign) + ")\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = Convert.ToByte(0);
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    RequestDataEvent?.Invoke(RequestDataRead);

                    break;

                //--------------------------------
                case SYNCHRONIZE:

                    SynchronizeRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // часы
                    SynchronizeRead.bHour = bBufRead[2];                            //* 2 *//

                    // минуты
                    SynchronizeRead.bMin = bBufRead[3];                             //* 3 *//

                    // секунды
                    SynchronizeRead.bSec = bBufRead[4];                             //* 4 *//               

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Синхронизация\n";

                    strInform = "Время " + Convert.ToString(SynchronizeRead.bHour) + ":" + Convert.ToString(SynchronizeRead.bMin) + ":" + Convert.ToString(SynchronizeRead.bSec) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = Convert.ToByte(0);
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    SynchronizeEvent?.Invoke(SynchronizeRead);

                    break;

                //--------------------------------                
                case TEXT_MESSAGE:

                    TextMessageRead.Init();

                    // адрес отправителя, всего пакетов    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[1];                                         //* 1 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    btaResult.CopyTo(bByteBit, 0);

                    // адрес получателя 
                    iAdrReceive = Convert.ToInt32(bByteBit[0]);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[1];                                         //* 1 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TextMessageRead.bCountPack = bByteBit[0];

                    // номер текущего пакета
                    TextMessageRead.bTempPack = bBufRead[2];                        //* 2 *//      

                    if (TextMessageRead.bTempPack == 1)
                    {
                        TextMessageRead.strText = "";
                        for (int i = 0; i < 150; i++)
                            TextMessageRead.bText[i] = 0;
                    }

                    // текст
                    string strTextTemp = "";
                    byte[] bTextTemp = new byte[9];

                    for (int i = 0; i < 9; i++)
                    {
                        TextMessageRead.bText[(TextMessageRead.bTempPack - 1) * 9 + i] = bBufRead[i + 3];
                    }

                    for (int i = 0; i < 9; i++)
                    {
                        bTextTemp[i] = bBufRead[i + 3];
                        if ((bTextTemp[i] > 127) & (bTextTemp[i] <= 175))
                            bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 64));
                        else
                        {
                            if ((bTextTemp[i] > 223) & (bTextTemp[i] < 241))
                                bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 16));
                        }
                    }
                    strTextTemp = Encoding.Default.GetString(bTextTemp);
                    strTextTemp = strTextTemp.TrimEnd('\0');
                    TextMessageRead.strText = TextMessageRead.strText + strTextTemp;
                    strTextTemp = strTextTemp + "\n";

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Текстовое сообщение\n";

                    strInform = strInform + "Всего пакетов " + Convert.ToString(TextMessageRead.bCountPack + "\n");
                    strInform = strInform + "Номер пакета " + Convert.ToString(TextMessageRead.bTempPack + "\n");
                    strInform = strInform + strTextTemp;
                    if (TextMessageRead.bTempPack == TextMessageRead.bCountPack)
                        strInform = strInform + "Весь текст " + TextMessageRead.strText + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = TextMessageRead.bTempPack;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    TextMessageEvent?.Invoke(TextMessageRead);

                    break;

                //--------------------------------
                case REGIME_WORK:

                    RegimeWorkRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // режим
                    RegimeWorkRead.bRegime = bBufRead[2];                           //* 2 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Режим работы\n";

                    switch (RegimeWorkRead.bRegime)
                    {
                        case 0:
                            strInform = "Подготовка (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;

                        case 1:
                            strInform = "Радиоразведка (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;

                        case 2:
                            strInform = "Радиоподавление (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;

                        case 3:
                            strInform = "Тренаж РР (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;

                        case 4:
                            strInform = "Тренаж РП (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;

                        default:
                            strInform = "Неопределен (код " + Convert.ToString(RegimeWorkRead.bRegime) + ")\n";
                            break;
                    }

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = Convert.ToByte(0);
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    RegimeWorkEvent?.Invoke(RegimeWorkRead);

                    break;

                //--------------------------------
                case FORBID_RANGE_FREQ:

                    ForbidRangeFreqRead.Init();

                    result = new byte[4];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    ForbidRangeFreqRead.bAction = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    ForbidRangeFreqRead.bTempRow = bByteBit[0];

                    // частота минимальная  
                    result = new byte[4];

                    result[3] = bBufRead[3];                                       //* 3 *//

                    result[2] = bBufRead[4];                                       //* 4 *//

                    result[1] = bBufRead[5];                                       //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ForbidRangeFreqRead.iFreqBegin = BitConverter.ToInt32(result, 0);

                    // частота максимальная                
                    result = new byte[4];

                    result[3] = bBufRead[6];                                       //* 3 *//

                    result[2] = bBufRead[7];                                       //* 4 *//

                    result[1] = bBufRead[8];                                       //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ForbidRangeFreqRead.iFreqEnd = BitConverter.ToInt32(result, 0);


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Запрещенные частоты\n";

                    if (ForbidRangeFreqRead.bAction == 0)
                        strInform = "Добавить (код " + Convert.ToString(ForbidRangeFreqRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 1)
                        strInform = "Заменить (код " + Convert.ToString(ForbidRangeFreqRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 2)
                        strInform = "Удалить (код " + Convert.ToString(ForbidRangeFreqRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 4)
                        strInform = "Очистить (код " + Convert.ToString(ForbidRangeFreqRead.bAction) + ")\n";

                    strInform = strInform + "Номер строки " + Convert.ToString(ForbidRangeFreqRead.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(ForbidRangeFreqRead.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(ForbidRangeFreqRead.iFreqEnd) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = ForbidRangeFreqRead.bTempRow;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    ForbidRangeFreqEvent?.Invoke(ForbidRangeFreqRead);

                    break;


                //--------------------------------
                case RECON_SECTOR_RANGE:

                    ReconSectorRangeRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    ReconSectorRangeRead.bAction = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    ReconSectorRangeRead.bTempRow = bByteBit[0];

                    // угол минимальный
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    result[1] = bBufRead[3];                                        //* 3 *//
                    bByteBit[0] = bBufRead[4];                                      //* 4 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ReconSectorRangeRead.wAngleBegin = BitConverter.ToUInt16(result, 0);


                    // угол максимальный
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[4];                                      //* 4 *//
                    bByte2Bit[1] = bBufRead[5];                                      //* 5 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[9];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ReconSectorRangeRead.wAngleEnd = BitConverter.ToUInt16(result, 0);

                    // частота минимальная 
                    result = new byte[4];

                    result[3] = bBufRead[6];                                        //* 6 *//

                    result[2] = bBufRead[7];                                        //* 7 *//

                    result[1] = bBufRead[8];                                        //* 8 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ReconSectorRangeRead.iFreqBegin = BitConverter.ToInt32(result, 0);

                    // частота максимальная                
                    result = new byte[4];

                    result[3] = bBufRead[9];                                        //* 9 *//

                    result[2] = bBufRead[10];                                       //* 10 *//

                    result[1] = bBufRead[11];                                       //* 11 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ReconSectorRangeRead.iFreqEnd = BitConverter.ToInt32(result, 0);


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Сектора и диапазоны РР\n";

                    if (ReconSectorRangeRead.bAction == 0)
                        strInform = "Добавить (код " + Convert.ToString(ReconSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 1)
                        strInform = "Заменить (код " + Convert.ToString(ReconSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 2)
                        strInform = "Удалить (код " + Convert.ToString(ReconSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 4)
                        strInform = "Очистить (код " + Convert.ToString(ReconSectorRangeRead.bAction) + ")\n";

                    strInform = strInform + "Номер строки " + Convert.ToString(ReconSectorRangeRead.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(ReconSectorRangeRead.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(ReconSectorRangeRead.iFreqEnd) + "\n";
                    strInform = strInform + "Угол мин. " + Convert.ToString(ReconSectorRangeRead.wAngleBegin) + "\n";
                    strInform = strInform + "Угол макс. " + Convert.ToString(ReconSectorRangeRead.wAngleEnd) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = ReconSectorRangeRead.bTempRow;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    ReconSectorRangeEvent?.Invoke(ReconSectorRangeRead);

                    break;

                //--------------------------------
                case SUPRESS_SECTOR_RANGE:
                    SupressSectorRangeRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressSectorRangeRead.bAction = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressSectorRangeRead.bTempRow = bByteBit[0];

                    // угол минимальный
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    result[1] = bBufRead[3];                                        //* 3 *//
                    bByteBit[0] = bBufRead[4];                                      //* 4 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressSectorRangeRead.wAngleBegin = BitConverter.ToUInt16(result, 0);


                    // угол максимальный
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[4];                                      //* 4 *//
                    bByte2Bit[1] = bBufRead[5];                                      //* 5 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[9];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressSectorRangeRead.wAngleEnd = BitConverter.ToUInt16(result, 0);

                    // приоритет
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                      //* 5 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressSectorRangeRead.bPrior = bByteBit[0];

                    // частота минимальная 
                    result = new byte[4];

                    result[3] = bBufRead[6];                                        //* 6 *//

                    result[2] = bBufRead[7];                                        //* 7 *//

                    result[1] = bBufRead[8];                                        //* 8 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressSectorRangeRead.iFreqBegin = BitConverter.ToInt32(result, 0);

                    // частота максимальная                
                    result = new byte[4];

                    result[3] = bBufRead[9];                                        //* 9 *//

                    result[2] = bBufRead[10];                                       //* 10 *//

                    result[1] = bBufRead[11];                                       //* 11 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressSectorRangeRead.iFreqEnd = BitConverter.ToInt32(result, 0);

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Сектора и диапазоны РП\n";

                    if (ReconSectorRangeRead.bAction == 0)
                        strInform = "Добавить (код " + Convert.ToString(SupressSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 1)
                        strInform = "Заменить (код " + Convert.ToString(SupressSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 2)
                        strInform = "Удалить (код " + Convert.ToString(SupressSectorRangeRead.bAction) + ")\n";
                    if (ForbidRangeFreqRead.bAction == 4)
                        strInform = "Очистить (код " + Convert.ToString(SupressSectorRangeRead.bAction) + ")\n";

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressSectorRangeRead.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(SupressSectorRangeRead.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(SupressSectorRangeRead.iFreqEnd) + "\n";
                    strInform = strInform + "Угол мин. " + Convert.ToString(SupressSectorRangeRead.wAngleBegin) + "\n";
                    strInform = strInform + "Угол макс. " + Convert.ToString(SupressSectorRangeRead.wAngleEnd) + "\n";
                    strInform = strInform + "Приоритет " + Convert.ToString(SupressSectorRangeRead.bPrior) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = SupressSectorRangeRead.bTempRow;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    SupressSectorRangeEvent?.Invoke(SupressSectorRangeRead);

                    break;

                //--------------------------------
                case SUPRESS_FWS:
                    SupressFWSRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFWSRead.bAction = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFWSRead.bTempRow = bByteBit[0];

                    // частота РП             
                    result = new byte[4];

                    result[3] = bBufRead[3];                                        //* 3 *//
                    result[2] = bBufRead[4];                                        //* 4 *//
                    result[1] = bBufRead[5];                                        //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressFWSRead.iFreq = BitConverter.ToInt32(result, 0);

                    // код параметров помехи    
                    result = new byte[4];

                    result[3] = bBufRead[6];                                        //* 6 *//
                    result[2] = bBufRead[7];                                        //* 7 *//
                    result[1] = bBufRead[8];                                        //* 8 *//
                    result[0] = bBufRead[9];                                        //* 9 *//


                    SupressFWSRead.bCodeModulation = bBufRead[7];                   //* 7 *//
                    SupressFWSRead.bCodeDeviation = bBufRead[8];                   //* 8 *//
                    SupressFWSRead.bCodeManipulation = bBufRead[9];                   //* 9 *//


                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressFWSRead.iCodeHind = BitConverter.ToInt32(result, 0);

                    // приоритет
                    SupressFWSRead.bPrior = bBufRead[10];                           //* 10 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Назначение ФЧ на РП\n";

                    if (SupressFWSRead.bAction == 0)
                        strInform = strInform + "Добавить (код " + Convert.ToString(SupressFWSRead.bAction) + ")\n";
                    if (SupressFWSRead.bAction == 1)
                        strInform = strInform + "Заменить (код " + Convert.ToString(SupressFWSRead.bAction) + ")\n";
                    if (SupressFWSRead.bAction == 2)
                        strInform = strInform + "Удалить (код " + Convert.ToString(SupressFWSRead.bAction) + ")\n";
                    if (SupressFWSRead.bAction == 4)
                        strInform = strInform + "Очистить (код " + Convert.ToString(SupressFWSRead.bAction) + ")\n";

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressFWSRead.bTempRow) + "\n";
                    strInform = strInform + "Частота " + Convert.ToString(SupressFWSRead.iFreq) + "\n";

                    strInform = strInform + "Параметры помехи " + GetParamHind(SupressFWSRead.bCodeModulation, SupressFWSRead.bCodeDeviation, SupressFWSRead.bCodeManipulation) + "\n";

                    strInform = strInform + "Код помехи " + Convert.ToString(SupressFWSRead.iCodeHind) + "\n";
                    strInform = strInform + "Приоритет " + Convert.ToString(SupressFWSRead.bPrior) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = SupressFWSRead.bTempRow;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    SupressFWSEvent?.Invoke(SupressFWSRead);

                    break;

                //--------------------------------
                case SUPRESS_FHSS:
                    SupressFHSSRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bAction = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bTempRow = bByteBit[0];

                    // частота минимальная         
                    result = new byte[4];

                    result[3] = bBufRead[3];                                        //* 3 *//
                    result[2] = bBufRead[4];                                        //* 4 *//
                    result[1] = bBufRead[5];                                        //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressFHSSRead.iFreqBegin = BitConverter.ToInt32(result, 0);

                    // частота максимальная  
                    result = new byte[4];

                    result[3] = bBufRead[6];                                        //* 6 *//
                    result[2] = bBufRead[7];                                        //* 7 *//
                    result[1] = bBufRead[8];                                        //* 8 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SupressFHSSRead.iFreqEnd = BitConverter.ToInt32(result, 0);

                    // длительность                         
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[9];                                         //* 9 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bGroupDuration = bByteBit[0];

                    btaResult[0] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[9];                                         //* 9 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bDuration = bByteBit[0];

                    // код вида модуляции         
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[10];                                        //* 10 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bCodeModulation = bByteBit[0];

                    // код шага сетки частот                    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[10];                                        //* 10 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[5];
                    btaResult[1] = btaTemp[6];
                    btaResult[2] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bCodeStep = bByteBit[0];

                    // параметры помехи
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[11];                                         //* 11 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bCodeDeviation = bByteBit[0];

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 11 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SupressFHSSRead.bCodeManipulation = bByteBit[0];

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Назначение ППРЧ на РП\n";

                    if (SupressFHSSRead.bAction == 0)
                        strInform = strInform + "Добавить (код " + Convert.ToString(SupressFHSSRead.bAction) + ")\n";
                    if (SupressFHSSRead.bAction == 1)
                        strInform = strInform + "Заменить (код " + Convert.ToString(SupressFHSSRead.bAction) + ")\n";
                    if (SupressFHSSRead.bAction == 2)
                        strInform = strInform + "Удалить (код " + Convert.ToString(SupressFHSSRead.bAction) + ")\n";
                    if (SupressFHSSRead.bAction == 4)
                        strInform = strInform + "Очистить (код " + Convert.ToString(SupressFHSSRead.bAction) + ")\n";

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressFHSSRead.bTempRow) + "\n";
                    strInform = strInform + "Частота мин." + Convert.ToString(SupressFHSSRead.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс." + Convert.ToString(SupressFHSSRead.iFreqEnd) + "\n";

                    if (SupressFHSSRead.bGroupDuration == 0)
                        strInform = strInform + "Длительность " + Convert.ToString(Convert.ToDouble(SupressFHSSRead.bDuration) / 10.0) + "\n";
                    else
                        strInform = strInform + "Длительность " + Convert.ToString(SupressFHSSRead.bDuration) + "\n";

                    strInform = strInform + "Параметры помехи " + GetParamHind(SupressFHSSRead.bCodeModulation, SupressFHSSRead.bCodeDeviation, SupressFHSSRead.bCodeManipulation) + "\n";

                    if (SupressFHSSRead.bCodeStep == 1)
                        strInform = strInform + "Шаг <= 5 кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";
                    if (SupressFHSSRead.bCodeStep == 2)
                        strInform = strInform + "Шаг (5-12,5] кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";
                    if (SupressFHSSRead.bCodeStep == 3)
                        strInform = strInform + "Шаг (12,5-25] кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";
                    if (SupressFHSSRead.bCodeStep == 4)
                        strInform = strInform + "Шаг (25-125] кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";
                    if (SupressFHSSRead.bCodeStep == 5)
                        strInform = strInform + "Шаг (125-3000] кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";
                    if (SupressFHSSRead.bCodeStep == 6)
                        strInform = strInform + "Шаг >3000 кГц (код " + Convert.ToString(SupressFHSSRead.bCodeStep) + ")\n";


                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = SupressFHSSRead.bTempRow;
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    SupressFHSSEvent?.Invoke(SupressFHSSRead);

                    break;

                //--------------------------------
                case REQUEST_BEARING:
                    RequestBearingRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//                   

                    // признак запроса                   
                    RequestBearingRead.bSign = bBufRead[2];                         //* 2 *//                    

                    // частота          
                    result = new byte[4];

                    result[3] = bBufRead[3];                                        //* 3 *//
                    result[2] = bBufRead[4];                                        //* 4 *//
                    result[1] = bBufRead[5];                                        //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    RequestBearingRead.iFreq = BitConverter.ToInt32(result, 0);

                    // время пеленгования                
                    RequestBearingRead.bTime = bBufRead[6];                         //* 6 *//   

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Запрос на ИП или КвП\n";

                    if (RequestBearingRead.bSign == 0)
                        strInform = strInform + "Квазиодновременное пеленгование (код " + Convert.ToString(RequestBearingRead.bSign) + ")\n";
                    if (RequestBearingRead.bSign == 1)
                        strInform = strInform + "Исполнительное пеленгование (код " + Convert.ToString(RequestBearingRead.bSign) + ")\n";

                    strInform = strInform + "Частота " + Convert.ToString(RequestBearingRead.iFreq) + "\n";
                    strInform = strInform + "Время " + Convert.ToString(RequestBearingRead.bTime) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    //if (chAnswer.Checked)
                    if (_СlaimСheck)
                    {
                        iCodeCmd = 11;

                        AnsReceptionSend.Init();

                        AnsReceptionSend.bCmd = Convert.ToByte(iNumCmd);
                        AnsReceptionSend.bTempPack = Convert.ToByte(0);
                        AnsReceptionSend.bCodeError = Convert.ToByte(0);

                        SendCommandToAKPP(iCodeCmd, iAdrReceive);
                    }
                    OnReadByte?.Invoke();
                    RequestBearingEvent?.Invoke(RequestBearingRead);

                    break;

                //--------------------------------                                            
                case TEST_CHANNEL:

                    TestChannelRead.Init();

                    // адрес получателя 
                    iAdrReceive = bBufRead[1];                                      //* 1 *//      

                    // контроль канала    
                    TestChannelRead.bSignDir = bBufRead[2];                         //* 2 *//

                    // исправность литер ведущей АСП 
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    // интервал передачи 
                    TestChannelRead.bTime = bBufRead[7];                            //* 7 *//

                    // исправность ПУ
                    TestChannelRead.bServicePC = bBufRead[8];                       //* 8 *//

                    // режим ПУ
                    TestChannelRead.bRegimePC = bBufRead[9];                        //* 9 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceive.ToString() + "\n";
                    strTypeCmd = "Тестирование канала\n";

                    strInform = strInform + "Контроль канала  " + Convert.ToString(TestChannelRead.bSignDir) + "\n";


                    switch (TestChannelRead.bRegimeMain)
                    {
                        case 0:
                            strInform = strInform + "Режим ПУ  Подготовка  (код " + Convert.ToString(TestChannelRead.bRegimePC) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Режим ПУ  Радиоразведка  (код " + Convert.ToString(TestChannelRead.bRegimePC) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Режим ПУ  Радиоподавление  (код " + Convert.ToString(TestChannelRead.bRegimePC) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Режим ПУ  Тренаж РР  (код " + Convert.ToString(TestChannelRead.bRegimePC) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Режим ПУ  Тренаж РП  (код " + Convert.ToString(TestChannelRead.bRegimePC) + ")\n";
                            break;
                    }

                    strInform += "Исправность ПУ " + Convert.ToString(TestChannelRead.bServicePC) + "\n";



                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;


            }

            iRes = -1;

            return iRes;
        }
    }
}
