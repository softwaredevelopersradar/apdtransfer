﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        // Запрос от ПУ АКПП
        public struct TRequestData
        {
            public byte bSign;           // признак запроса

            public void Init()
            {
                bSign = 0;
            }
        }

        // Синхронизация от ПУ АКПП
        public struct TSynchronize
        {
            public byte bHour;           // часы
            public byte bMin;            // минуты
            public byte bSec;            // секунды

            public void Init()
            {
                bHour = 0;
                bMin = 0;
                bSec = 0;
            }
        }

        // Текстовое сообщение от ПУ АКПП/ на ПУ АКПП
        public struct TTextMessage
        {
            public byte bCountPack;      // количество пакетов
            public byte bTempPack;       // номер текущего пакета
            public string strText;         // текст сообщения
            public byte[] bText;         // текст сообщения массив байт

            public void Init()
            {
                bCountPack = 0;
                bTempPack = 0;
                //strText = "";
                //for (int i = 0; i < 150; i++)
                //  bText[i] = 0;
                bText = new byte[150];
            }
        }

        // Режим от ПУ АКПП
        public struct TRegimeWork
        {
            public byte bRegime;         // режим  

            public void Init()
            {
                bRegime = 0;
            }
        }

        // Запрещенные частоты (диапазоны) от ПУ АКПП/ на ПУ АКПП
        public struct TForbidRangeFreq
        {
            public byte bAction;          // действие
            public byte bTempRow;         // номер строки
            public int iFreqBegin;       // начальная частота
            public int iFreqEnd;         // конечная частота

            public void Init()
            {
                bAction = 0;
                bTempRow = 0;
                iFreqBegin = 0;
                iFreqEnd = 0;
            }
        }

        // Сектора и диапазоны РР от ПУ АКПП
        public struct TReconSectorRange
        {
            public byte bAction;          // действие
            public byte bTempRow;         // номер строки
            public ushort wAngleBegin;      // начальный угол
            public ushort wAngleEnd;        // конечный угол
            public int iFreqBegin;       // начальная частота
            public int iFreqEnd;         // конечная частота

            public void Init()
            {
                bAction = 0;
                bTempRow = 0;
                wAngleBegin = 0;
                wAngleEnd = 0;
                iFreqBegin = 0;
                iFreqEnd = 0;
            }
        }

        // Сектора и диапазоны РП от ПУ АКПП
        public struct TSupressSectorRange
        {
            public byte bAction;         // действие
            public byte bTempRow;        // номер строки
            public ushort wAngleBegin;     // начальный угол
            public ushort wAngleEnd;       // конечный угол
            public byte bPrior;          // приоритет
            public int iFreqBegin;      // начальная частота
            public int iFreqEnd;        // конечная частота

            public void Init()
            {
                bAction = 0;
                bTempRow = 0;
                wAngleBegin = 0;
                wAngleEnd = 0;
                bPrior = 0;
                iFreqBegin = 0;
                iFreqEnd = 0;
            }
        }

        // ФРЧ на подавление от ПУ АКПП
        public struct TSupressFWS
        {
            public byte bAction;             // действие
            public byte bTempRow;            // номер строки            
            public int iFreq;               // частота
            public int iCodeHind;           // код помехи
            public byte bPrior;              // приоритет
            public byte bCodeModulation;     // код вида
            public byte bCodeDeviation;      // код ширины
            public byte bCodeManipulation;   // код скорости

            public void Init()
            {
                bAction = 0;
                bTempRow = 0;
                iFreq = 0;
                iCodeHind = 0;
                bPrior = 0;
                bCodeModulation = 0;
                bCodeDeviation = 0;
                bCodeManipulation = 0;
            }
        }

        // ППРЧ на подавление от ПУ АКПП
        public struct TSupressFHSS
        {
            public byte bAction;             // действие
            public byte bTempRow;            // номер строки            
            public int iFreqBegin;          // начальная частота
            public int iFreqEnd;            // конечная частота 
            public byte bGroupDuration;      // длительность
            public byte bDuration;           // длительность            
            public byte bCodeStep;           // код шага
            public byte bCodeModulation;     // код вида
            public byte bCodeDeviation;      // код ширины
            public byte bCodeManipulation;   // код ширины

            public void Init()
            {
                bAction = 0;
                bTempRow = 0;
                iFreqBegin = 0;
                iFreqEnd = 0;
                bDuration = 0;
                bCodeModulation = 0;
                bCodeStep = 0;
                bCodeDeviation = 0;
                bCodeManipulation = 0;
            }
        }

        // Запрос на ИП или КвП ФЧ от ПУ АКПП
        public struct TRequestBearing
        {
            public byte bSign;           // признак запроса
            public int iFreq;           // частота   
            public byte bTime;           // время ожидания ответа

            public void Init()
            {
                bSign = 0;
                iFreq = 0;
                bTime = 0;
            }
        }

        // Квитанция подтверждения приема на ПУ АКПП
        public struct TAnsReception
        {
            public byte bCodeError;      // код ошибки           
            public byte bCmd;            // шифр кдг
            public byte bTempPack;       // номер пакета в кдг

            public void Init()
            {
                bCodeError = 0;
                bCmd = 0;
                bTempPack = 0;
            }
        }

        // Координаты АСП на ПУ АКПП
        public struct TCoordJammer
        {
            public byte bSignLong;         // знак долготы           
            public byte bLongDegree;       // градусы долготы
            public byte bLongMinute;       // минуты долготы
            public byte bLongSecond;       // секунды долготы
            public byte bLongPartSecond;   // сотые доли секунд долготы
            public byte bSignLat;          // знак широты
            public byte bLatDegree;        // градусы широты
            public byte bLatMinute;        // минуты широты
            public byte bLatSecond;        // секунды широты
            public byte bLatPartSecond;    // сотые доли секунд широты

            public void Init()
            {
                bSignLong = 0;
                bLongDegree = 0;
                bLongMinute = 0;
                bLongSecond = 0;
                bSignLat = 0;
                bLatDegree = 0;
                bLatMinute = 0;
                bLatSecond = 0;
            }
        }

        // Сведения об ИРИ ФЧ на ПУ АКПП (часть 1)
        public struct TDataFWSPartOne
        {
            public byte bTempSource;     // номер текущего ИРИ
            public int iFreq;           // частота           
            public byte bCodeType;       // код вида            
            public byte bCodeWidth;      // код ширины
            public byte bHour;           // часы
            public byte bMin;            // минуты
            public byte bSec;            // секунды
            public byte bCodeRate;       // скорость ТЛГ

            public void Init()
            {
                bTempSource = 0;
                iFreq = 0;
                bCodeType = 0;
                bCodeWidth = 0;
                bHour = 0;
                bMin = 0;
                bSec = 0;
                bCodeRate = 0;
            }
        }

        // Сведения об ИРИ ФЧ на ПУ АКПП (часть 2)
        public struct TDataFWSPartTwo
        {
            public byte bTempSource;      // номер текущего ИРИ
            public byte bSignLong;        // знак долготы           
            public byte bLongDegree;      // градусы долготы
            public byte bLongMinute;      // минуты долготы
            public byte bLongSecond;      // секунды долготы
            public byte bSignLat;         // знак широты
            public byte bLatDegree;       // градусы широты
            public byte bLatMinute;       // минуты широты
            public byte bLatSecond;       // секунды широты
            public ushort wBearMain;        // пеленг от ведущей
            public ushort wBearAdd;         // пеленг от ведомой


            public void Init()
            {
                bTempSource = 0;
                bSignLong = 0;
                bLongDegree = 0;
                bLongMinute = 0;
                bLongSecond = 0;
                bSignLat = 0;
                bLatDegree = 0;
                bLatMinute = 0;
                bLatSecond = 0;
                wBearMain = 0;
                wBearAdd = 0;

            }
        }

        // Сведения об ИРИ ППРЧ на ПУ АКПП (часть 1)
        public struct TDataFHSSPartOne
        {
            public byte bTempSource;     // номер текущего ИРИ
            public int iFreqBegin;      // начальная частота
            public int iFreqEnd;        // конечная частота
            public byte bHour;           // часы
            public byte bMin;            // минуты
            public byte bSec;            // секунды
            public byte bCodeWidth;      // код ширины
            public byte bCodeStep;       // код шага

            public void Init()
            {
                bTempSource = 0;
                iFreqBegin = 0;
                iFreqEnd = 0;
                bHour = 0;
                bMin = 0;
                bSec = 0;
                bCodeWidth = 0;
                bCodeStep = 0;
            }
        }

        // Сведения об ИРИ ППРЧ на ПУ АКПП (часть 2)
        public struct TDataFHSSPartTwo
        {
            public byte bTempSource;     // номер текущего ИРИ
            public byte bGroupDuration;  // длительность
            public byte bDuration;       // длительность
            public ushort wBearMain;       // пеленг от ведущей
            public ushort wBearAdd;        // пеленг от ведомой
            public byte bSignLong;       // знак долготы           
            public byte bLongDegree;     // градусы долготы
            public byte bLongMinute;     // минуты долготы
            public byte bLongSecond;     // секунды долготы
            public byte bSignLat;        // знак широты
            public byte bLatDegree;      // градусы широты
            public byte bLatMinute;      // минуты широты
            public byte bLatSecond;      // секунды широты   
            public byte bCodeType;       // код вида модуляции           

            public void Init()
            {
                bTempSource = 0;
                bGroupDuration = 0;
                bDuration = 0;
                wBearMain = 0;
                wBearAdd = 0;
                bSignLong = 0;
                bLongDegree = 0;
                bLongMinute = 0;
                bLongSecond = 0;
                bSignLat = 0;
                bLatDegree = 0;
                bLatMinute = 0;
                bLatSecond = 0;
            }
        }

        // Квитанция о состоянии подавляемых частот
        public struct TStateSupressFreq
        {
            public byte[] bStateWork;      // инф-ция о работе    
            public byte[] bStateLongTerm;  // инф-ция о долговременности           
            public byte[] bStateSupress;   // инф-ция о подвлении           
            public byte bTempSubRange;   // номер поддиапазона
            public byte bSighF;          // признак ФЧ/ППРЧ
            public byte bStateFHSS;      // инф-ция ППРЧ

            public void Init()
            {
                bStateWork = new byte[10];

                bStateWork[0] = 0;
                bStateWork[1] = 0;
                bStateWork[2] = 0;
                bStateWork[3] = 0;
                bStateWork[4] = 0;
                bStateWork[5] = 0;
                bStateWork[6] = 0;
                bStateWork[7] = 0;
                bStateWork[8] = 0;
                bStateWork[9] = 0;

                bStateLongTerm = new byte[10];

                bStateLongTerm[0] = 0;
                bStateLongTerm[1] = 0;
                bStateLongTerm[2] = 0;
                bStateLongTerm[3] = 0;
                bStateLongTerm[4] = 0;
                bStateLongTerm[5] = 0;
                bStateLongTerm[6] = 0;
                bStateLongTerm[7] = 0;
                bStateLongTerm[8] = 0;
                bStateLongTerm[9] = 0;

                bStateSupress = new byte[10];

                bStateSupress[0] = 0;
                bStateSupress[1] = 0;
                bStateSupress[2] = 0;
                bStateSupress[3] = 0;
                bStateSupress[4] = 0;
                bStateSupress[5] = 0;
                bStateSupress[6] = 0;
                bStateSupress[7] = 0;
                bStateSupress[8] = 0;
                bStateSupress[9] = 0;

                bTempSubRange = 0;
                bSighF = 0;
                bStateFHSS = 0;
            }
        }

        // Ответ на исполнительное пеленгование
        public struct TExecuteBearing
        {
            public ushort wBear;            // пеленг   

            public void Init()
            {
                wBear = 0;
            }
        }

        // Ответ на квазиодновременное пеленгование на ПУ АКПП
        public struct TSimultanBearing
        {
            public ushort wBearMain;       // пеленг от ведущей
            public ushort wBearAdd;        // пеленг от ведомой
            public byte bSignLong;       // знак долготы           
            public byte bLongDegree;     // градусы долготы
            public byte bLongMinute;     // минуты долготы
            public byte bLongSecond;     // секунды долготы
            public byte bSignLat;        // знак широты
            public byte bLatDegree;      // градусы широты
            public byte bLatMinute;      // минуты широты
            public byte bLatSecond;      // секунды широты    

            public void Init()
            {
                wBearMain = 0;
                wBearAdd = 0;
                bSignLong = 0;
                bLongDegree = 0;
                bLongMinute = 0;
                bLongSecond = 0;
                bSignLat = 0;
                bLatDegree = 0;
                bLatMinute = 0;
                bLatSecond = 0;
            }
        }

        // Тестирование канала связи на ПУ АКПП
        public struct TTestChannel
        {
            public byte bSignDir;        // контроль канала            
            public byte[] bLetterMain;     // исправность литер ведущей
            public byte[] bLetterAdd;      // исправность литер ведомой
            public byte bRegimeMain;     // режим работы ведущей
            public byte bRegimeAdd;      // режим работы ведомой
            public byte bRegimePC;       // режим работы ПУ
            public byte bServicePC;      // исправность ПУ
            public byte bTime;           // интервал передачи

            public void Init()
            {
                bSignDir = 0;

                bLetterMain = new byte[7];

                bLetterMain[0] = 0;
                bLetterMain[1] = 0;
                bLetterMain[2] = 0;
                bLetterMain[3] = 0;
                bLetterMain[4] = 0;
                bLetterMain[5] = 0;
                bLetterMain[6] = 0;

                bLetterAdd = new byte[7];

                bLetterAdd[0] = 0;
                bLetterAdd[1] = 0;
                bLetterAdd[2] = 0;
                bLetterAdd[3] = 0;
                bLetterAdd[4] = 0;
                bLetterAdd[5] = 0;
                bLetterAdd[6] = 0;
                bRegimeMain = 0;
                bRegimeAdd = 0;
                bRegimePC = 0;
                bServicePC = 0;
            }
        }




    }
}
