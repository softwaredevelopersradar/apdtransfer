﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        private int SendCommandToJammer(int iNumCmd, int iAdrReceiver)
        {
            int iRes = -1;

            for (int i = 0; i < 12; i++)
                bBufSend[i] = 0;

            byte[] bTemp = new byte[1];
            byte[] bByteBit = new byte[1];

            byte[] intBytes;
            byte[] result;

            string strTimeCodeAdr = "";
            string strTypeCmd = "";
            string strInform = "";

            switch (iNumCmd)
            {

                //--------------------------------
                case REQUEST_DATA:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак запроса
                    bBufSend[2] = Convert.ToByte(RequestDataSend.bSign);        //* 2 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Запрос данных\n";

                    switch (RequestDataSend.bSign)
                    {
                        case 0:
                            strInform = strInform + "Координаты (код " + Convert.ToString(RequestDataSend.bSign) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "ИРИ ФЧ (код " + Convert.ToString(RequestDataSend.bSign) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "ИРИ ППРЧ (код " + Convert.ToString(RequestDataSend.bSign) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Квитанция о подавляемых ИРИ (код " + Convert.ToString(RequestDataSend.bSign) + ")\n";
                            break;

                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(RequestDataSend.bSign) + ")\n";
                            break;
                    }

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case SYNCHRONIZE:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                          //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                     //* 1 *//

                    // часы
                    bBufSend[2] = Convert.ToByte(SynchronizeSend.bHour);            //* 2 *//

                    // минуты
                    bBufSend[3] = Convert.ToByte(SynchronizeSend.bMin);             //* 3 *//

                    // секунды
                    bBufSend[4] = Convert.ToByte(SynchronizeSend.bSec);             //* 4 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Синхронизация\n";

                    strInform = strInform + "Время " + Convert.ToString(SynchronizeSend.bHour) + ":" + Convert.ToString(SynchronizeSend.bMin) + ":" + Convert.ToString(SynchronizeSend.bSec) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                
                case TEXT_MESSAGE:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                          //* 0 *//

                    // адрес отправителя, всего пакетов    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrReceiver);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = TextMessageSend.bCountPack;
                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                      //* 1 *//

                    // номер текущего пакета
                    bBufSend[2] = TextMessageSend.bTempPack;                        //* 2 *//                      

                    // текст
                    string strTextTemp = "";

                    for (int i = 0; i < 9; i++)
                    {
                        bBufSend[i + 3] = TextMessageSend.bText[(TextMessageSend.bTempPack - 1) * 9 + i];
                    }

                    byte[] bTextTemp = new byte[9];
                    for (int i = 0; i < 9; i++)
                    {
                        bTextTemp[i] = bBufSend[i + 3];
                        if ((bTextTemp[i] > 127) & (bTextTemp[i] <= 175))
                            bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 64));
                        else
                        {
                            if ((bTextTemp[i] > 223) & (bTextTemp[i] < 241))
                                bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 16));
                        }
                    }
                    strTextTemp = Encoding.Default.GetString(bTextTemp) + "\n";

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Текстовое сообщение\n";

                    strInform = strInform + "Всего пакетов " + Convert.ToString(TextMessageSend.bCountPack + "\n");
                    strInform = strInform + "Номер пакета " + Convert.ToString(TextMessageSend.bTempPack + "\n");
                    strInform = strInform + strTextTemp;

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case REGIME_WORK:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // режим
                    bBufSend[2] = Convert.ToByte(RegimeWorkSend.bRegime);        //* 2 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Режим работы\n";

                    switch (RegimeWorkSend.bRegime)
                    {
                        case 0:
                            strInform = strInform + "Подготовка (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Радиоразведка (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Радиоподавление (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Тренаж РР (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Тренаж РП (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(RegimeWorkSend.bRegime) + ")\n";
                            break;
                    }
                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case FORBID_RANGE_FREQ:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(ForbidRangeFreqSend.bAction);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(ForbidRangeFreqSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                      //* 2 *//

                    // частота минимальная                   
                    intBytes = BitConverter.GetBytes(ForbidRangeFreqSend.iFreqBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                       //* 3 *//

                    bBufSend[4] = result[2];                                       //* 4 *//

                    bBufSend[5] = result[1];                                       //* 5 *//

                    // частота максимальная                
                    intBytes = BitConverter.GetBytes(ForbidRangeFreqSend.iFreqEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[6] = result[3];                                       //* 6 *//

                    bBufSend[7] = result[2];                                       //* 7 *//

                    bBufSend[8] = result[1];                                       //* 8 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Запрещенные частоты\n";

                    switch (ForbidRangeFreqSend.bAction)
                    {
                        case 0:
                            strInform = strInform + "Добавить (код " + Convert.ToString(ForbidRangeFreqSend.bAction) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Заменить (код " + Convert.ToString(ForbidRangeFreqSend.bAction) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Удалить (код " + Convert.ToString(ForbidRangeFreqSend.bAction) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Очистить (код " + Convert.ToString(ForbidRangeFreqSend.bAction) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(ForbidRangeFreqSend.bAction) + ")\n";
                            break;
                    }

                    strInform = strInform + "Номер строки " + Convert.ToString(ForbidRangeFreqSend.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(ForbidRangeFreqSend.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(ForbidRangeFreqSend.iFreqEnd) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;


                //--------------------------------
                case RECON_SECTOR_RANGE:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(ReconSectorRangeSend.bAction);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(ReconSectorRangeSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                      //* 2 *//

                    // угол минимальный
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.wAngleBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                      //* 3 *//

                    // угол минимальный, угол максимальный
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.wAngleBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.wAngleEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];
                    btaResult[6] = btaTemp[13];
                    btaResult[7] = btaTemp[14];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                      //* 4 *//


                    // угол максимальный
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.wAngleEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                      //* 5 *//

                    // частота минимальная                   
                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.iFreqBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[6] = result[3];                                        //* 6 *//

                    bBufSend[7] = result[2];                                        //* 7 *//

                    bBufSend[8] = result[1];                                        //* 8 *//

                    // частота максимальная                
                    intBytes = BitConverter.GetBytes(ReconSectorRangeSend.iFreqEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[9] = result[3];                                         //* 9 *//

                    bBufSend[10] = result[2];                                        //* 10 *//

                    bBufSend[11] = result[1];                                        //* 11 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Сектора и диапазоны РР\n";

                    switch (ReconSectorRangeSend.bAction)
                    {
                        case 0:
                            strInform = strInform + "Добавить (код " + Convert.ToString(ReconSectorRangeSend.bAction) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Заменить (код " + Convert.ToString(ReconSectorRangeSend.bAction) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Удалить (код " + Convert.ToString(ReconSectorRangeSend.bAction) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Очистить (код " + Convert.ToString(ReconSectorRangeSend.bAction) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(ReconSectorRangeSend.bAction) + ")\n";
                            break;
                    }

                    strInform = strInform + "Номер строки " + Convert.ToString(ReconSectorRangeSend.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(ReconSectorRangeSend.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(ReconSectorRangeSend.iFreqEnd) + "\n";
                    strInform = strInform + "Угол мин. " + Convert.ToString(ReconSectorRangeSend.wAngleBegin) + "\n";
                    strInform = strInform + "Угол макс. " + Convert.ToString(ReconSectorRangeSend.wAngleEnd) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case SUPRESS_SECTOR_RANGE:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressSectorRangeSend.bAction);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressSectorRangeSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                      //* 2 *//

                    // угол минимальный
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.wAngleBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                      //* 3 *//

                    // угол минимальный, угол максимальный
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.wAngleBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.wAngleEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];
                    btaResult[6] = btaTemp[13];
                    btaResult[7] = btaTemp[14];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                      //* 4 *//


                    // угол максимальный, приоритет
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.wAngleEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressSectorRangeSend.bPrior);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                      //* 5 *//

                    // частота минимальная                   
                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.iFreqBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[6] = result[3];                                        //* 6 *//

                    bBufSend[7] = result[2];                                        //* 7 *//

                    bBufSend[8] = result[1];                                        //* 8 *//

                    // частота максимальная                
                    intBytes = BitConverter.GetBytes(SupressSectorRangeSend.iFreqEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[9] = result[3];                                         //* 9 *//

                    bBufSend[10] = result[2];                                        //* 10 *//

                    bBufSend[11] = result[1];                                        //* 11 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Сектора и диапазоны РП\n";

                    switch (SupressSectorRangeSend.bAction)
                    {
                        case 0:
                            strInform = strInform + "Добавить (код " + Convert.ToString(SupressSectorRangeSend.bAction) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Заменить (код " + Convert.ToString(SupressSectorRangeSend.bAction) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Удалить (код " + Convert.ToString(SupressSectorRangeSend.bAction) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Очистить (код " + Convert.ToString(SupressSectorRangeSend.bAction) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(SupressSectorRangeSend.bAction) + ")\n";
                            break;
                    }

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressSectorRangeSend.bTempRow) + "\n";
                    strInform = strInform + "Частота мин. " + Convert.ToString(SupressSectorRangeSend.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс. " + Convert.ToString(SupressSectorRangeSend.iFreqEnd) + "\n";
                    strInform = strInform + "Угол мин. " + Convert.ToString(SupressSectorRangeSend.wAngleBegin) + "\n";
                    strInform = strInform + "Угол макс. " + Convert.ToString(SupressSectorRangeSend.wAngleEnd) + "\n";
                    strInform = strInform + "Приоритет " + Convert.ToString(SupressSectorRangeSend.bPrior) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case SUPRESS_FWS:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFWSSend.bAction);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFWSSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                      //* 2 *//                    

                    // частота РП                 
                    intBytes = BitConverter.GetBytes(SupressFWSSend.iFreq);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                        //* 3 *//

                    bBufSend[4] = result[2];                                        //* 4 *//

                    bBufSend[5] = result[1];                                        //* 5 *//

                    // код параметров помехи        

                    bBufSend[6] = 0;                                                //* 6 *//

                    bBufSend[7] = SupressFWSSend.bCodeModulation;                   //* 7 *//

                    bBufSend[8] = SupressFWSSend.bCodeDeviation;                    //* 8 *//

                    bBufSend[9] = SupressFWSSend.bCodeManipulation;                 //* 9 *//


                    // приоритет
                    bBufSend[10] = Convert.ToByte(SupressFWSSend.bPrior);           //* 10 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Назначение ФЧ на РП\n";

                    switch (SupressFWSSend.bAction)
                    {
                        case 0:
                            strInform = strInform + "Добавить (код " + Convert.ToString(SupressFWSSend.bAction) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Заменить (код " + Convert.ToString(SupressFWSSend.bAction) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Удалить (код " + Convert.ToString(SupressFWSSend.bAction) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Очистить (код " + Convert.ToString(SupressFWSSend.bAction) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(SupressFWSSend.bAction) + ")\n";
                            break;
                    }

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressFWSSend.bTempRow) + "\n";
                    strInform = strInform + "Частота " + Convert.ToString(SupressFWSSend.iFreq) + "\n";
                    strInform = strInform + "Приоритет " + Convert.ToString(SupressFWSSend.bPrior) + "\n";

                    strInform = strInform + "Параметры помехи " + GetParamHind(SupressFWSSend.bCodeModulation, SupressFWSSend.bCodeDeviation, SupressFWSSend.bCodeManipulation) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case SUPRESS_FHSS:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                      //* 0 *//

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                 //* 1 *//

                    // признак, номер строки изменения
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bAction);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                      //* 2 *//                    

                    // частота минимальная               
                    intBytes = BitConverter.GetBytes(SupressFHSSSend.iFreqBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                        //* 3 *//
                    bBufSend[4] = result[2];                                        //* 4 *//
                    bBufSend[5] = result[1];                                        //* 5 *//

                    // частота максимальная          
                    intBytes = BitConverter.GetBytes(SupressFHSSSend.iFreqEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[6] = result[3];                                        //* 6 *//
                    bBufSend[7] = result[2];                                        //* 7 *//
                    bBufSend[8] = result[1];                                        //* 8 *//

                    // длительность                         
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bGroupDuration);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bTempRow);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                      //* 9 *//    

                    // код вида модуляции, код шага сетки частот                    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bCodeModulation);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bCodeStep);

                    btaTemp = new BitArray(bTemp);

                    btaResult[5] = btaTemp[0];
                    btaResult[6] = btaTemp[1];
                    btaResult[7] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[10] = bByteBit[0];                                      //* 10 *//   

                    // параметры помехи

                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bCodeDeviation);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SupressFHSSSend.bCodeManipulation);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[11] = bByteBit[0];                                      //* 11 *//          


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Назначение ППРЧ на РП\n";

                    switch (SupressFHSSSend.bAction)
                    {
                        case 0:
                            strInform = strInform + "Добавить (код " + Convert.ToString(SupressFHSSSend.bAction) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Заменить (код " + Convert.ToString(SupressFHSSSend.bAction) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Удалить (код " + Convert.ToString(SupressFHSSSend.bAction) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Очистить (код " + Convert.ToString(SupressFHSSSend.bAction) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(SupressFHSSSend.bAction) + ")\n";
                            break;
                    }

                    strInform = strInform + "Номер строки " + Convert.ToString(SupressFHSSSend.bTempRow) + "\n";
                    strInform = strInform + "Частота мин." + Convert.ToString(SupressFHSSSend.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс." + Convert.ToString(SupressFHSSSend.iFreqEnd) + "\n";
                    strInform = strInform + "Длительность " + Convert.ToString(SupressFHSSSend.bDuration) + "\n";

                    strInform = strInform + "Параметры помехи " + GetParamHind(SupressFHSSSend.bCodeModulation, SupressFHSSSend.bCodeDeviation, SupressFHSSSend.bCodeManipulation) + "\n";

                    switch (SupressFHSSSend.bCodeStep)
                    {
                        case 1:
                            strInform = strInform + "Шаг 5 кГц (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Шаг 12,5 кГц (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Шаг 25 кГц (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Шаг 125 кГц (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                        case 5:
                            strInform = strInform + "Шаг 3000 кГц (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Шаг неопределен (код " + Convert.ToString(SupressFHSSSend.bCodeStep) + ")\n";
                            break;
                    }



                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case REQUEST_BEARING:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                          //* 0 *//

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                     //* 1 *//

                    // признак запроса                   
                    bBufSend[2] = Convert.ToByte(RequestBearingSend.bSign);         //* 2 *//                    

                    // частота минимальная               
                    intBytes = BitConverter.GetBytes(RequestBearingSend.iFreq);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                        //* 3 *//

                    bBufSend[4] = result[2];                                        //* 4 *//

                    bBufSend[5] = result[1];                                        //* 5 *//

                    // время пеленгования                
                    bBufSend[6] = Convert.ToByte(RequestBearingSend.bTime);         //* 6 *//   

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Запрос на ИП или КвП\n";

                    switch (RequestBearingSend.bSign)
                    {
                        case 0:
                            strInform = strInform + "Квазиодновременное пеленгование (код " + Convert.ToString(RequestBearingSend.bSign) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Исполнительное пеленгование (код " + Convert.ToString(RequestBearingSend.bSign) + ")\n";
                            break;
                        default:
                            strInform = strInform + "Неопределен (код " + Convert.ToString(RequestBearingSend.bSign) + ")\n";
                            break;
                    }

                    strInform = strInform + "Частота " + Convert.ToString(RequestBearingSend.iFreq) + "\n";
                    strInform = strInform + "Время " + Convert.ToString(RequestBearingSend.bTime) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;
                //--------------------------------
                default:
                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                          //* 0 *//

                    // адрес получателя 
                    bBufSend[1] = Convert.ToByte(iAdrReceiver);                     //* 1 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrReceiver.ToString() + "\n";
                    strTypeCmd = "Кодограмма неопределена\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;
            }
            try
            {
                spPortAKPP.Write(bBufSend, 0, 12);

                //if (chBytes.Checked)
                if (true)
                {
                    string strResHex = string.Empty;
                    string strResBin = string.Empty;
                    int iDecValue = 0;
                    strInform = "";

                    for (int i = 0; i < 12; i++)
                    {
                        iDecValue = 0;
                        iDecValue = bBufSend[i];
                        strResHex = iDecValue.ToString("X2");
                        strResBin = Convert.ToString(iDecValue, 2);
                        while (strResBin.Length < 8)
                        {
                            strResBin = "0" + strResBin;
                        }
                        //rtbLogMessage.SelectionColor = Color.Indigo;
                        if (i <= 8)
                            strInform = strInform + " " + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                        else
                            strInform = strInform + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                    }

                    SetWriteByte(strInform, 12);
                }

                iRes = -1;
            }
            catch (System.Exception)
            {
                iRes = 0;
            }
            return iRes;
        }
    }
}
