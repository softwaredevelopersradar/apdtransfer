﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        // This delegate enables asynchronous calls for setting 
        // the text property on a TextBox control. 
        delegate void SetTextCallback(string strTimeCodeAdr, string strTypeCmd, string strInform);

        delegate void SetByteCallback(string strBytes, int iCountByte);

        private void SetReadText(string strTimeCodeAdr, string strTypeCmd, string strInform)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            //if (this.rtbLogMessage.InvokeRequired)
            //{
            //    SetTextCallback d = new SetTextCallback(SetReadText);
            //    this.BeginInvoke(d, new object[] { strTimeCodeAdr, strTypeCmd, strInform });
            //}
            //else
            //{
            //    rtbLogMessage.AppendText("\n");
            //    rtbLogMessage.SelectionColor = Color.Red;
            //    rtbLogMessage.AppendText(strTimeCodeAdr);
            //    rtbLogMessage.SelectionColor = Color.Red;
            //    rtbLogMessage.AppendText(strTypeCmd);
            //    rtbLogMessage.SelectionColor = Color.Black;
            //    rtbLogMessage.AppendText(strInform);
            //    rtbLogMessage.SelectionStart = rtbLogMessage.TextLength;
            //    rtbLogMessage.ScrollToCaret();
            //}
        }

        private void SetReadByte(string strBytes, int iCountByte)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            //if (this.rtbLogMessage.InvokeRequired)
            //{
            //    SetByteCallback d = new SetByteCallback(SetReadByte);
            //    this.BeginInvoke(d, new object[] { strBytes, iCountByte });
            //}
            //else
            //{
            //    rtbLogMessage.AppendText("\n");
            //    rtbLogMessage.SelectionColor = Color.Red;
            //    rtbLogMessage.AppendText("Принято  " + Convert.ToString(iCountByte) + "  байт \n");
            //    rtbLogMessage.SelectionColor = Color.Indigo;
            //    rtbLogMessage.AppendText(strBytes);
            //    rtbLogMessage.SelectionStart = rtbLogMessage.TextLength;
            //    rtbLogMessage.ScrollToCaret();
            //}
        }
    }
}
