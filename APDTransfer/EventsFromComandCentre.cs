﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        public delegate void RequestDataEventHandler(TRequestData RequestDataRead);
        public event RequestDataEventHandler RequestDataEvent;

        public delegate void SynchronizeEventHandler(TSynchronize SynchronizeRead);
        public event SynchronizeEventHandler SynchronizeEvent;

        public delegate void TextMessageEventHandler(TTextMessage TextMessageRead);
        public event TextMessageEventHandler TextMessageEvent;

        public delegate void RegimeWorkEventHandler(TRegimeWork RegimeWorkRead);
        public event RegimeWorkEventHandler RegimeWorkEvent;

        public delegate void ForbidRangeFreqEventHandler(TForbidRangeFreq ForbidRangeFreqRead);
        public event ForbidRangeFreqEventHandler ForbidRangeFreqEvent;

        public delegate void ReconSectorRangeEventHandler(TReconSectorRange ReconSectorRangeRead);
        public event ReconSectorRangeEventHandler ReconSectorRangeEvent;

        public delegate void SupressSectorRangeEventHandler(TSupressSectorRange SupressSectorRangeRead);
        public event SupressSectorRangeEventHandler SupressSectorRangeEvent;

        public delegate void SupressFWSEventHandler(TSupressFWS SupressFWSRead);
        public event SupressFWSEventHandler SupressFWSEvent;
        
        public delegate void SupressFHSSEventHandler(TSupressFHSS SupressFHSSRead);
        public event SupressFHSSEventHandler SupressFHSSEvent;

        public delegate void RequestBearingEventHandler(TRequestBearing RequestBearingRead);
        public event RequestBearingEventHandler RequestBearingEvent;
    }
}
