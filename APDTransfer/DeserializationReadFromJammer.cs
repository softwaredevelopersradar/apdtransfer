﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        private int ReadCommandFromJammer(int iNumCmd, byte[] bBufRead)
        {
            int iRes = -1;

            for (int i = 0; i < 12; i++)
                bBufSend[i] = 0;

            byte[] bTemp = new byte[1];
            byte[] bByteBit = new byte[1];
            byte[] bByte2Bit = new byte[2];

            byte[] result;

            string strTimeCodeAdr = "";
            string strTypeCmd = "";
            string strInform = "";

            int iAdrSender = -1;

            switch (iNumCmd)
            {

                //--------------------------------                
                case TEXT_MESSAGE:

                    TextMessageRead.Init();

                    // адрес отправителя    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[1];                                         //* 1 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    btaResult.CopyTo(bByteBit, 0);

                    iAdrSender = Convert.ToInt32(bByteBit[0]);

                    // количество пакетов
                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[1];                                         //* 1 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TextMessageRead.bCountPack = bByteBit[0];

                    // номер текущего пакета
                    TextMessageRead.bTempPack = bBufRead[2];                        //* 2 *//    

                    if (TextMessageRead.bTempPack == 1)
                    {
                        TextMessageRead.strText = "";
                        for (int i = 0; i < 150; i++)
                            TextMessageRead.bText[i] = 0;
                    }

                    // текст
                    if (TextMessageRead.bTempPack == 1)
                        TextMessageRead.strText = "";

                    string strTextTemp = "";
                    byte[] bTextTemp = new byte[9];

                    for (int i = 0; i < 9; i++)
                    {
                        TextMessageRead.bText[(TextMessageRead.bTempPack - 1) * 9 + i] = bBufRead[i + 3];
                    }

                    for (int i = 0; i < 9; i++)
                    {
                        bTextTemp[i] = bBufRead[i + 3];
                        if ((bTextTemp[i] > 127) & (bTextTemp[i] <= 175))
                            bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 64));
                        else
                        {
                            if ((bTextTemp[i] > 223) & (bTextTemp[i] < 241))
                                bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 16));
                        }
                    }
                    strTextTemp = Encoding.Default.GetString(bTextTemp);
                    strTextTemp = strTextTemp.TrimEnd('\0');
                    TextMessageRead.strText = TextMessageRead.strText + strTextTemp;
                    strTextTemp = strTextTemp + "\n";

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Текстовое сообщение\n";

                    strInform = strInform + "Всего пакетов " + Convert.ToString(TextMessageRead.bCountPack + "\n");
                    strInform = strInform + "Номер пакета " + Convert.ToString(TextMessageRead.bTempPack + "\n");
                    strInform = strInform + strTextTemp;
                    if (TextMessageRead.bTempPack == TextMessageRead.bCountPack)
                        strInform = strInform + "\n Весь текст " + TextMessageRead.strText + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    TextMessageJEvent?.Invoke(TextMessageRead);

                    break;

                //--------------------------------                                
                case ANS_RECEPTION:

                    AnsReceptionRead.Init();

                    // адрес отправителя 
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // код ошибки          
                    AnsReceptionRead.bCodeError = bBufRead[2];                      //* 2 *//

                    // шифр принятой кодограммы    
                    AnsReceptionRead.bCmd = bBufRead[3];                            //* 3 *//

                    // номер пакета (строки, ИРИ) 
                    AnsReceptionRead.bTempPack = bBufRead[4];                       //* 4 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Квитанция подтверждения\n";

                    strInform = strInform + "Код ошибки " + Convert.ToString(AnsReceptionRead.bCodeError + "\n");
                    strInform = strInform + "Шифр кдг " + Convert.ToString(AnsReceptionRead.bCmd + "\n");
                    strInform = strInform + "Номер пакета (строки, ИРИ) " + Convert.ToString(AnsReceptionRead.bTempPack + "\n");

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    ReceptionEvent?.Invoke(AnsReceptionRead);

                    break;

                //--------------------------------                                
                case COORD_JAMMER:

                    CoordJammerRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // знак долготы
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    CoordJammerRead.bSignLong = bByteBit[0];

                    // градусы долготы  
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[2];                                      //* 2 *//
                    bByte2Bit[1] = bBufRead[3];                                      //* 3 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    CoordJammerRead.bLongDegree = bByteBit[0];

                    // минуты долготы  
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//           
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];
                    btaResult[1] = btaTemp[3];
                    btaResult[2] = btaTemp[4];
                    btaResult[3] = btaTemp[5];
                    btaResult[4] = btaTemp[6];
                    btaResult[5] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    CoordJammerRead.bLongMinute = bByteBit[0];

                    // секунды долготы    
                    CoordJammerRead.bLongSecond = bBufRead[4];                      //* 4 *//

                    // знак широты
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                         //* 5 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    CoordJammerRead.bSignLat = bByteBit[0];

                    // градусы широты 
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                         //* 5 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    CoordJammerRead.bLatDegree = bByteBit[0];

                    // минуты широты  
                    CoordJammerRead.bLatMinute = bBufRead[6];                       //* 6 *//

                    // секунды широты   
                    CoordJammerRead.bLatSecond = bBufRead[7];                       //* 7 *//

                    // сотые доли секунд долготы
                    CoordJammerRead.bLongPartSecond = bBufRead[8];                  //* 8 *//

                    // сотые доли секунд широты  
                    CoordJammerRead.bLatPartSecond = bBufRead[9];                  //* 9 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Координаты СП\n";

                    strInform = strInform + Convert.ToString(CoordJammerRead.bLongDegree) + " " + Convert.ToString(CoordJammerRead.bLongMinute) +
                                             " " + Convert.ToString(CoordJammerRead.bLongSecond) + "," + Convert.ToString(CoordJammerRead.bLongPartSecond);
                    if (CoordJammerRead.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(CoordJammerRead.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(CoordJammerRead.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(CoordJammerRead.bLatDegree) + " " + Convert.ToString(CoordJammerRead.bLatMinute) +
                                             " " + Convert.ToString(CoordJammerRead.bLatSecond) + "," + Convert.ToString(CoordJammerRead.bLatPartSecond);
                    if (CoordJammerRead.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(CoordJammerRead.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(CoordJammerRead.bSignLat) + ")\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    CoordJammerEvent?.Invoke(CoordJammerRead);

                    break;

                //--------------------------------                                
                case DATA_FWS_PART_ONE:

                    DataFWSPartOneRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // номер ИРИ
                    DataFWSPartOneRead.bTempSource = bBufRead[2];                   //* 2 *//

                    // частота                   
                    result = new byte[4];

                    result[3] = bBufRead[3];                                        //* 3 *//
                    result[2] = bBufRead[4];                                        //* 4 *//
                    result[1] = bBufRead[5];                                        //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFWSPartOneRead.iFreq = BitConverter.ToInt32(result, 0);

                    // код вида модуляции
                    DataFWSPartOneRead.bCodeType = bBufRead[6];                     //* 6 *//

                    // код ширины
                    DataFWSPartOneRead.bCodeWidth = bBufRead[7];                    //* 7 *//

                    // часы
                    DataFWSPartOneRead.bHour = bBufRead[8];                         //* 8 *//

                    // минуты
                    DataFWSPartOneRead.bMin = bBufRead[9];                          //* 9 *//

                    // секунды
                    DataFWSPartOneRead.bSec = bBufRead[10];                         //* 10 *//

                    // код скорости ТЛГ
                    DataFWSPartOneRead.bCodeRate = bBufRead[11];                    //* 11 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ФЧ 1\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFWSPartOneRead.bTempSource) + "\n";
                    strInform = strInform + "Частота " + Convert.ToString(DataFWSPartOneRead.iFreq) + "\n";

                    switch (DataFWSPartOneRead.bCodeType)
                    {
                        case 1:
                            strInform = strInform + "Модуляция АМ (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Модуляция ЧМ (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Модуляция ЧМ-2 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Модуляция НБП (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Модуляция ВБП (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Модуляция ШПС (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Модуляция нес (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Модуляция неопр (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 9:
                            strInform = strInform + "Модуляция АМ-2 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 10:
                            strInform = strInform + "Модуляция ФМ-2 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 11:
                            strInform = strInform + "Модуляция ФМ-4 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 12:
                            strInform = strInform + "Модуляция ФМ-8 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 13:
                            strInform = strInform + "Модуляция КАМ-16 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 14:
                            strInform = strInform + "Модуляция КАМ-32 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 15:
                            strInform = strInform + "Модуляция КАМ-64 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 16:
                            strInform = strInform + "Модуляция КАМ-128 (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;

                        case 17:
                            strInform = strInform + "Модуляция ППРЧ (код " + Convert.ToString(DataFWSPartOneRead.bCodeType) + ")\n";
                            break;
                    }

                    switch (DataFWSPartOneRead.bCodeWidth)
                    {
                        case 0:
                            strInform = strInform + "Ширина <= 5 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 1:
                            strInform = strInform + "Ширина 5-12,5 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Ширина 12,5-25 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Ширина 25-125 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Ширина 125-3000 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Ширина 3000-3500 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Ширина 3500-8330 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Ширина > 8330 кГц (код " + Convert.ToString(DataFWSPartOneRead.bCodeWidth) + ")\n";
                            break;
                    }

                    strInform = strInform + "Время  " + Convert.ToString(DataFWSPartOneRead.bHour) + ":"
                                             + Convert.ToString(DataFWSPartOneRead.bMin) + ":" + Convert.ToString(DataFWSPartOneRead.bSec) + "\n";

                    switch (DataFWSPartOneRead.bCodeRate)
                    {
                        case 1:
                            strInform = strInform + "Скорость < 10 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Скорость 10-20 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Скорость 20-50 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Скорость 50-100 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 5:
                            strInform = strInform + "Скорость 100-200 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 6:
                            strInform = strInform + "Скорость 200-500 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 7:
                            strInform = strInform + "Скорость 500-1 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 8:
                            strInform = strInform + "Скорость 1 000-2 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 9:
                            strInform = strInform + "Скорость 2 000-5 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 10:
                            strInform = strInform + "Скорость 5 000-10 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 11:
                            strInform = strInform + "Скорость 10 000-20 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 12:
                            strInform = strInform + "Скорость 20 000-50 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 13:
                            strInform = strInform + "Скорость 50 000-100 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 14:
                            strInform = strInform + "Скорость 100 000-200 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 15:
                            strInform = strInform + "Скорость 200 000-500 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 16:
                            strInform = strInform + "Скорость 500 000-1 000 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 17:
                            strInform = strInform + "Скорость 1 000 000-2 000 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 18:
                            strInform = strInform + "Скорость 2 000 000-5 000 000 бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                        case 19:
                            strInform = strInform + "Скорость > 5 000 000  бод (код " + Convert.ToString(DataFWSPartOneRead.bCodeRate) + ")\n";
                            break;
                    }

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    DataFWS1Event?.Invoke(DataFWSPartOneRead);

                    break;

                //--------------------------------
                case DATA_FWS_PART_TWO:

                    DataFWSPartOneRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // номер ИРИ
                    DataFWSPartTwoRead.bTempSource = bBufRead[2];                   //* 2 *//

                    // знак долготы
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFWSPartTwoRead.bSignLong = bByteBit[0];

                    // градусы долготы  
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[3];                                      //* 3 *//
                    bByte2Bit[1] = bBufRead[4];                                      //* 4 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFWSPartTwoRead.bLongDegree = bByteBit[0];

                    // минуты долготы  
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[4];                                         //* 4 *//           
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];
                    btaResult[1] = btaTemp[3];
                    btaResult[2] = btaTemp[4];
                    btaResult[3] = btaTemp[5];
                    btaResult[4] = btaTemp[6];
                    btaResult[5] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFWSPartTwoRead.bLongMinute = bByteBit[0];

                    // секунды долготы    
                    DataFWSPartTwoRead.bLongSecond = bBufRead[5];                   //* 5 *//

                    // знак широты
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFWSPartTwoRead.bSignLat = bByteBit[0];

                    // градусы широты 
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFWSPartTwoRead.bLatDegree = bByteBit[0];

                    // минуты широты  
                    DataFWSPartTwoRead.bLatMinute = bBufRead[7];                    //* 7 *//

                    // секунды широты   
                    DataFWSPartTwoRead.bLatSecond = bBufRead[8];                    //* 8 *//

                    // пеленг от ведущей
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    result[1] = bBufRead[9];                                        //* 9 *//
                    bByteBit[0] = bBufRead[10];                                     //* 10 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFWSPartTwoRead.wBearMain = BitConverter.ToUInt16(result, 0);


                    // пеленг от ведомой
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[10];                                      //* 10 *//
                    bByte2Bit[1] = bBufRead[11];                                      //* 11 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFWSPartTwoRead.wBearAdd = BitConverter.ToUInt16(result, 0);

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ФЧ 2\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFWSPartTwoRead.bTempSource) + "\n";

                    strInform = strInform + Convert.ToString(DataFWSPartTwoRead.bLongDegree) + " " + Convert.ToString(DataFWSPartTwoRead.bLongMinute) +
                                             " " + Convert.ToString(DataFWSPartTwoRead.bLongSecond);
                    if (DataFWSPartTwoRead.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(DataFWSPartTwoRead.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(DataFWSPartTwoRead.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(DataFWSPartTwoRead.bLatDegree) + " " + Convert.ToString(DataFWSPartTwoRead.bLatMinute) +
                                             " " + Convert.ToString(DataFWSPartTwoRead.bLatSecond);
                    if (DataFWSPartTwoRead.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(DataFWSPartTwoRead.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(DataFWSPartTwoRead.bSignLat) + ")\n";

                    strInform = strInform + "Пеленг от ведущей " + Convert.ToString(DataFWSPartTwoRead.wBearMain) + "\n";

                    strInform = strInform + "Пеленг от ведомой " + Convert.ToString(DataFWSPartTwoRead.wBearAdd) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    DataFWS2Event?.Invoke(DataFWSPartTwoRead);

                    break;


                //--------------------------------                                
                case DATA_FHSS_PART_ONE:

                    DataFHSSPartOneRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // номер ИРИ
                    DataFHSSPartOneRead.bTempSource = bBufRead[2];                   //* 2 *//

                    // частота минимальная                                             
                    result = new byte[4];

                    result[3] = bBufRead[3];                                        //* 3 *//
                    result[2] = bBufRead[4];                                        //* 4 *//
                    result[1] = bBufRead[5];                                        //* 5 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFHSSPartOneRead.iFreqBegin = BitConverter.ToInt32(result, 0);

                    // частота максимальная                            
                    result = new byte[4];

                    result[3] = bBufRead[6];                                        //* 6 *//
                    result[2] = bBufRead[7];                                        //* 7 *//
                    result[1] = bBufRead[8];                                        //* 8 *//

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFHSSPartOneRead.iFreqEnd = BitConverter.ToInt32(result, 0);

                    // часы
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[9];                                        //* 9 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartOneRead.bHour = bByteBit[0];

                    // минуты   
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[9];                                     //* 9 *//
                    bByte2Bit[1] = bBufRead[10];                                    //* 10 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[6];
                    btaResult[1] = btaTemp[7];
                    btaResult[2] = btaTemp[8];
                    btaResult[3] = btaTemp[9];
                    btaResult[4] = btaTemp[10];
                    btaResult[5] = btaTemp[11];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartOneRead.bMin = bByteBit[0];

                    //секунды   
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[10];                                     //* 10 *//
                    bByte2Bit[1] = bBufRead[11];                                    //* 11 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];
                    btaResult[2] = btaTemp[6];
                    btaResult[3] = btaTemp[7];
                    btaResult[4] = btaTemp[8];
                    btaResult[5] = btaTemp[9];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartOneRead.bSec = bByteBit[0];

                    // код шага сетки
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[11];                                        //* 11 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];
                    btaResult[1] = btaTemp[3];
                    btaResult[2] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartOneRead.bCodeStep = bByteBit[0];

                    // код шага ширины    
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[11];                                        //* 11 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[5];
                    btaResult[1] = btaTemp[6];
                    btaResult[2] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartOneRead.bCodeWidth = bByteBit[0];

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ППРЧ 1\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFHSSPartOneRead.bTempSource) + "\n";
                    strInform = strInform + "Частота мин." + Convert.ToString(DataFHSSPartOneRead.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс." + Convert.ToString(DataFHSSPartOneRead.iFreqEnd) + "\n";
                    strInform = strInform + "Время  " + Convert.ToString(DataFHSSPartOneRead.bHour) + ":"
                                             + Convert.ToString(DataFHSSPartOneRead.bMin) + ":" + Convert.ToString(DataFHSSPartOneRead.bSec) + "\n";

                    switch (DataFHSSPartOneRead.bCodeStep)
                    {
                        case 1:
                            strInform = strInform + "Шаг < 5 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Шаг 2-12,5 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Шаг 12,5-25 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Шаг 25-125 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                        case 5:
                            strInform = strInform + "Шаг 125-3000 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                        case 6:
                            strInform = strInform + "Шаг > 3000 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeStep) + ")\n";
                            break;
                    }

                    switch (DataFHSSPartOneRead.bCodeWidth)
                    {
                        case 1:
                            strInform = strInform + "Ширина < 5 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Ширина 5-12,5 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Ширина 12,5-25 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Ширина 25-125 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Ширина 125-3000 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Ширина 3000-3500 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Ширина 3500-8330 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Ширина > 8330 кГц (код " + Convert.ToString(DataFHSSPartOneRead.bCodeWidth) + ")\n";
                            break;
                    }

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    DataFHSS1Event?.Invoke(DataFHSSPartOneRead);

                    break;

                //--------------------------------                                
                case DATA_FHSS_PART_TWO:

                    DataFHSSPartOneRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // номер ИРИ
                    DataFHSSPartTwoRead.bTempSource = bBufRead[2];                  //* 2 *//

                    // группа длительность
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bGroupDuration = bByteBit[0];

                    // длительность
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bDuration = bByteBit[0];

                    // пеленг от ведущей
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    result[1] = bBufRead[4];                                        //* 4 *//
                    bByteBit[0] = bBufRead[5];                                      //* 5 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFHSSPartTwoRead.wBearMain = BitConverter.ToUInt16(result, 0);

                    // пеленг от ведомой
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];

                    bByte2Bit[0] = bBufRead[5];                                      //* 5 *//
                    bByte2Bit[1] = bBufRead[6];                                      //* 6 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[9];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    DataFHSSPartTwoRead.wBearAdd = BitConverter.ToUInt16(result, 0);

                    // знак долготы
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bSignLong = bByteBit[0];

                    // градусы долготы
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[7];                                      //* 6 *//
                    bByte2Bit[1] = bBufRead[8];                                      //* 7 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[3];
                    btaResult[1] = btaTemp[4];
                    btaResult[2] = btaTemp[5];
                    btaResult[3] = btaTemp[6];
                    btaResult[4] = btaTemp[7];
                    btaResult[5] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLongMinute = bByteBit[0];

                    // минуты долготы
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[6];                                      //* 6 *//
                    bByte2Bit[1] = bBufRead[7];                                      //* 7 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[3];
                    btaResult[1] = btaTemp[4];
                    btaResult[2] = btaTemp[5];
                    btaResult[3] = btaTemp[6];
                    btaResult[4] = btaTemp[7];
                    btaResult[5] = btaTemp[8];
                    btaResult[6] = btaTemp[9];
                    btaResult[7] = btaTemp[10];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLongDegree = bByteBit[0];

                    // секунды долготы 
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[8];                                         //* 8 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLongSecond = bByteBit[0];

                    // знак широты
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[8];                                         //* 8 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bSignLat = bByteBit[0];

                    // градусы широты
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[9];                                         //* 9 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLatDegree = bByteBit[0];

                    // минуты широты
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[9];                                     //* 9 *//
                    bByte2Bit[1] = bBufRead[10];                                    //* 10 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[7];
                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLatMinute = bByteBit[0];

                    // секунды широты
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[10];                                    //* 10 *//
                    bByte2Bit[1] = bBufRead[11];                                    //* 11 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[5];
                    btaResult[1] = btaTemp[6];
                    btaResult[2] = btaTemp[7];
                    btaResult[3] = btaTemp[8];
                    btaResult[4] = btaTemp[9];
                    btaResult[5] = btaTemp[10];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bLatSecond = bByteBit[0];

                    // код вида модуляции 
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[11];                                         //* 11 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];
                    btaResult[1] = btaTemp[4];
                    btaResult[2] = btaTemp[5];
                    btaResult[3] = btaTemp[6];
                    btaResult[4] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    DataFHSSPartTwoRead.bCodeType = bByteBit[0];

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ППРЧ 2\n";


                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFHSSPartTwoRead.bTempSource) + "\n";
                    strInform = strInform + "Длительность " + Convert.ToString(DataFHSSPartTwoRead.bDuration) + " группа " + Convert.ToString(DataFHSSPartTwoRead.bGroupDuration) + "\n";

                    strInform = strInform + Convert.ToString(DataFHSSPartTwoRead.bLongDegree) + " " + Convert.ToString(DataFHSSPartTwoRead.bLongMinute) +
                                             " " + Convert.ToString(DataFHSSPartTwoRead.bLongSecond);
                    if (DataFHSSPartTwoSend.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(DataFHSSPartTwoRead.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(DataFHSSPartTwoRead.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(DataFHSSPartTwoRead.bLatDegree) + " " + Convert.ToString(DataFHSSPartTwoRead.bLatMinute) +
                                             " " + Convert.ToString(DataFHSSPartTwoRead.bLatSecond);
                    if (DataFHSSPartTwoSend.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(DataFHSSPartTwoRead.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(DataFHSSPartTwoRead.bSignLat) + ")\n";

                    strInform = strInform + "Пеленг от ведущей " + Convert.ToString(DataFHSSPartTwoRead.wBearMain) + "\n";

                    strInform = strInform + "Пеленг от ведомой " + Convert.ToString(DataFHSSPartTwoRead.wBearAdd) + "\n";

                    switch (DataFHSSPartTwoRead.bCodeType)
                    {
                        case 1:
                            strInform = strInform + "Модуляция АМ (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Модуляция ЧМ (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Модуляция ЧМ-2 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Модуляция НБП (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Модуляция ВБП (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Модуляция ШПС (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Модуляция нес (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Модуляция неопр (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 9:
                            strInform = strInform + "Модуляция АМ-2 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 10:
                            strInform = strInform + "Модуляция ФМ-2 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 11:
                            strInform = strInform + "Модуляция ФМ-4 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 12:
                            strInform = strInform + "Модуляция ФМ-8 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 13:
                            strInform = strInform + "Модуляция КАМ-16 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 14:
                            strInform = strInform + "Модуляция КАМ-32 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 15:
                            strInform = strInform + "Модуляция КАМ-64 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 16:
                            strInform = strInform + "Модуляция КАМ-128 (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;

                        case 17:
                            strInform = strInform + "Модуляция ППРЧ (код " + Convert.ToString(DataFHSSPartTwoRead.bCodeType) + ")\n";
                            break;
                    }

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    DataFHSS2Event?.Invoke(DataFHSSPartTwoRead);

                    break;

                //--------------------------------                                
                case STATE_SUPRESS_FREQ:

                    StateSupressFreqRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // инф-ция о ФЧ 1
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[0] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[0] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[0] = bByteBit[0];

                    // инф-ция о ФЧ 2
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[2];                                         //* 2 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[1] = bByteBit[0];

                    // инф-ция о ФЧ 3
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[2] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[2] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[2] = bByteBit[0];

                    // инф-ция о ФЧ 4
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[3] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[3] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[3] = bByteBit[0];

                    // инф-ция о ФЧ 5
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[4];                                         //* 4 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[4] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[4] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[4] = bByteBit[0];

                    // инф-ция о ФЧ 6
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[4];                                         //* 4 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[5] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[5] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[5] = bByteBit[0];

                    // инф-ция о ФЧ 7
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                         //* 5 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[6] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[6] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[6] = bByteBit[0];

                    // инф-ция о ФЧ 8
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                         //* 5 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[7] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[7] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[7] = bByteBit[0];

                    // инф-ция о ФЧ 9
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[8] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[8] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[8] = bByteBit[0];

                    // инф-ция о ФЧ 10
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateWork[9] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateLongTerm[9] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    StateSupressFreqRead.bStateSupress[9] = bByteBit[0];

                    // номер поддиапазона
                    StateSupressFreqRead.bTempSubRange = bBufRead[7];               //* 7 *//

                    // признак ППРЧ   
                    StateSupressFreqRead.bSighF = bBufRead[8];                      //* 8 *//

                    // работа ППРЧ   
                    StateSupressFreqRead.bStateFHSS = bBufRead[9];                  //* 9 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения о подавляемых ИРИ\n";


                    strInform = strInform + "Номер п/диапазона " + Convert.ToString(StateSupressFreqRead.bTempSubRange) + "\n";

                    strInform = strInform + "ФЧ 1 " + Convert.ToString(StateSupressFreqRead.bStateWork[0]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[0]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[0] + "\n");
                    strInform = strInform + "ФЧ 2 " + Convert.ToString(StateSupressFreqRead.bStateWork[1]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[1]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[1] + "\n");
                    strInform = strInform + "ФЧ 3 " + Convert.ToString(StateSupressFreqRead.bStateWork[2]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[2]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[2] + "\n");
                    strInform = strInform + "ФЧ 4 " + Convert.ToString(StateSupressFreqRead.bStateWork[3]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[3]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[3] + "\n");
                    strInform = strInform + "ФЧ 5 " + Convert.ToString(StateSupressFreqRead.bStateWork[4]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[4]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[4] + "\n");
                    strInform = strInform + "ФЧ 6 " + Convert.ToString(StateSupressFreqRead.bStateWork[5]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[5]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[5] + "\n");
                    strInform = strInform + "ФЧ 7 " + Convert.ToString(StateSupressFreqRead.bStateWork[6]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[6]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[6] + "\n");
                    strInform = strInform + "ФЧ 8 " + Convert.ToString(StateSupressFreqRead.bStateWork[7]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[7]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[7] + "\n");
                    strInform = strInform + "ФЧ 9 " + Convert.ToString(StateSupressFreqRead.bStateWork[8]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[8]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[8] + "\n");
                    strInform = strInform + "ФЧ 10 " + Convert.ToString(StateSupressFreqRead.bStateWork[9]) + "  " + Convert.ToString(StateSupressFreqRead.bStateLongTerm[9]) + "  "
                                            + Convert.ToString(StateSupressFreqRead.bStateSupress[9] + "\n");

                    strInform = strInform + "Признак ППРЧ " + Convert.ToString(StateSupressFreqRead.bSighF) + "\n";
                    strInform = strInform + "Состояние ППРЧ " + Convert.ToString(StateSupressFreqRead.bStateFHSS) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    StateSupressFreqEvent?.Invoke(StateSupressFreqRead);

                    break;

                //--------------------------------                                
                case EXECUTE_BEARING:

                    ExecuteBearingRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // пеленг  
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    result = new byte[2];
                    result[1] = bBufRead[2];                                        //* 4 *//
                    bByteBit[0] = bBufRead[3];                                      //* 5 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    ExecuteBearingRead.wBear = BitConverter.ToUInt16(result, 0);

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Исполнительное пеленгование\n";

                    strInform = strInform + "Пеленг " + Convert.ToString(ExecuteBearingRead.wBear) + "\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    ExecuteBearingEvent?.Invoke(ExecuteBearingRead);

                    break;

                //--------------------------------                                
                case SIMULTAN_BEARING:

                    SimultanBearingRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // пеленг от ведущей
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    result[1] = bBufRead[2];                                        //* 2 *//
                    bByteBit[0] = bBufRead[3];                                      //* 3 *//

                    btaTemp = new BitArray(bByteBit);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SimultanBearingRead.wBearMain = BitConverter.ToUInt16(result, 0);

                    // пеленг от ведомой
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[3];                                     //* 3 *//
                    bByte2Bit[1] = bBufRead[4];                                     //* 4 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[1] = bByteBit[0];

                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    result[0] = bByteBit[0];

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(result);
                    SimultanBearingRead.wBearAdd = BitConverter.ToUInt16(result, 0);

                    // знак долготы
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[5];                                         //* 5 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SimultanBearingRead.bSignLong = bByteBit[0];

                    // градусы долготы  
                    result = new byte[2];
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bByte2Bit[0] = bBufRead[5];                                      //* 5 *//
                    bByte2Bit[1] = bBufRead[6];                                      //* 6 *//                  
                    btaTemp = new BitArray(bByte2Bit);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = btaTemp[8];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SimultanBearingRead.bLongDegree = bByteBit[0];

                    // минуты долготы  
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[6];                                         //* 6 *//           
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];
                    btaResult[1] = btaTemp[3];
                    btaResult[2] = btaTemp[4];
                    btaResult[3] = btaTemp[5];
                    btaResult[4] = btaTemp[6];
                    btaResult[5] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SimultanBearingRead.bLongMinute = bByteBit[0];

                    // секунды долготы    
                    SimultanBearingRead.bLongSecond = bBufRead[7];                  //* 7 *//

                    // знак широты
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[8];                                         //* 8 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = false;
                    btaResult[2] = false;
                    btaResult[3] = false;
                    btaResult[4] = false;
                    btaResult[5] = false;
                    btaResult[6] = false;
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SimultanBearingRead.bSignLat = bByteBit[0];

                    // градусы широты 
                    btaTemp.SetAll(false);
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[8];                                         //* 8 *//
                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];
                    btaResult[5] = btaTemp[6];
                    btaResult[6] = btaTemp[7];
                    btaResult[7] = false;

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    SimultanBearingRead.bLatDegree = bByteBit[0];

                    // минуты широты  
                    SimultanBearingRead.bLatMinute = bBufRead[9];                   //* 9 *//

                    // секунды широты   
                    SimultanBearingRead.bLatSecond = bBufRead[10];                   //* 10 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Квазиодновременное пеленгование\n";

                    strInform = strInform + "Пеленг от ведущей  " + Convert.ToString(SimultanBearingRead.wBearMain) + "\n";
                    strInform = strInform + "Пеленг от ведомой  " + Convert.ToString(SimultanBearingRead.wBearAdd) + "\n";

                    strInform = strInform + Convert.ToString(SimultanBearingRead.bLongDegree) + " " + Convert.ToString(SimultanBearingRead.bLongMinute) +
                                             " " + Convert.ToString(SimultanBearingRead.bLongSecond);
                    if (SimultanBearingRead.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(SimultanBearingRead.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(SimultanBearingRead.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(SimultanBearingRead.bLatDegree) + " " + Convert.ToString(SimultanBearingRead.bLatMinute) +
                                             " " + Convert.ToString(SimultanBearingRead.bLatSecond);
                    if (SimultanBearingRead.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(SimultanBearingRead.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(SimultanBearingRead.bSignLat) + ")\n";

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    TSimultanBearingEvent?.Invoke(SimultanBearingRead);

                    break;

                //--------------------------------                                
                case TEST_CHANNEL:

                    TestChannelRead.Init();

                    // адрес отправителя   
                    iAdrSender = bBufRead[1];                                       //* 1 *//

                    // контроль канала    
                    TestChannelRead.bSignDir = bBufRead[2];                         //* 2 *//

                    // исправность литер ведущей АСП 
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    // литера 1
                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[3];                                         //* 3 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[0] = bByteBit[0];

                    // литера 2
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[1] = bByteBit[0];

                    // литера 3
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[2] = bByteBit[0];

                    // литера 4
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[3] = bByteBit[0];

                    // литера 5
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[4] = bByteBit[0];

                    // литера 6
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[5] = bByteBit[0];

                    // литера 7
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterMain[6] = bByteBit[0];

                    // исправность литер ведомой АСП
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    // литера 1
                    bTemp[0] = 0;
                    bTemp[0] = bBufRead[4];                                         //* 4 *//

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[0] = bByteBit[0];

                    // литера 2
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[1] = bByteBit[0];

                    // литера 3
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[2] = bByteBit[0];

                    // литера 4
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[3] = bByteBit[0];

                    // литера 5
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[4] = bByteBit[0];

                    // литера 6
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[5] = bByteBit[0];

                    // литера 7
                    btaResult.SetAll(false);

                    btaResult[0] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    TestChannelRead.bLetterAdd[6] = bByteBit[0];

                    // режим работы ведущей АСП   
                    TestChannelRead.bRegimeMain = bBufRead[5];                      //* 6 *//

                    // режим работы ведомой АСП  
                    TestChannelRead.bRegimeAdd = bBufRead[6];                      //* 6 *//

                    // интервал передачи 
                    TestChannelRead.bTime = bBufRead[7];                            //* 7 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Тестирование канала\n";

                    strInform = strInform + "Контроль канала  " + Convert.ToString(TestChannelRead.bSignDir) + "\n";
                    strInform = strInform + "Исправность литер ведущей СП \n";
                    strInform = strInform + "   Л1  " + Convert.ToString(TestChannelRead.bLetterMain[0]) + "\n";
                    strInform = strInform + "   Л2  " + Convert.ToString(TestChannelRead.bLetterMain[1]) + "\n";
                    strInform = strInform + "   Л3  " + Convert.ToString(TestChannelRead.bLetterMain[2]) + "\n";
                    strInform = strInform + "   Л4  " + Convert.ToString(TestChannelRead.bLetterMain[3]) + "\n";
                    strInform = strInform + "   Л5  " + Convert.ToString(TestChannelRead.bLetterMain[4]) + "\n";
                    strInform = strInform + "   Л6  " + Convert.ToString(TestChannelRead.bLetterMain[5]) + "\n";
                    strInform = strInform + "   Л7  " + Convert.ToString(TestChannelRead.bLetterMain[6]) + "\n";

                    strInform = strInform + "Исправность литер ведомой СП \n";
                    strInform = strInform + "   Л1  " + Convert.ToString(TestChannelRead.bLetterAdd[0]) + "\n";
                    strInform = strInform + "   Л2  " + Convert.ToString(TestChannelRead.bLetterAdd[1]) + "\n";
                    strInform = strInform + "   Л3  " + Convert.ToString(TestChannelRead.bLetterAdd[2]) + "\n";
                    strInform = strInform + "   Л4  " + Convert.ToString(TestChannelRead.bLetterAdd[3]) + "\n";
                    strInform = strInform + "   Л5  " + Convert.ToString(TestChannelRead.bLetterAdd[4]) + "\n";
                    strInform = strInform + "   Л6  " + Convert.ToString(TestChannelRead.bLetterAdd[5]) + "\n";
                    strInform = strInform + "   Л7  " + Convert.ToString(TestChannelRead.bLetterAdd[6]) + "\n";

                    switch (TestChannelRead.bRegimeMain)
                    {
                        case 0:
                            strInform = strInform + "Режим ведущей  Подготовка  (код " + Convert.ToString(TestChannelRead.bRegimeMain) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Режим ведущей  Радиоразведка  (код " + Convert.ToString(TestChannelRead.bRegimeMain) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Режим ведущей  Радиоподавление  (код " + Convert.ToString(TestChannelRead.bRegimeMain) + ")\n";
                            break;
                    }

                    switch (TestChannelRead.bRegimeAdd)
                    {
                        case 0:
                            strInform = strInform + "Режим ведомой  Подготовка  (код " + Convert.ToString(TestChannelRead.bRegimeAdd) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Режим ведомой  Радиоразведка  (код " + Convert.ToString(TestChannelRead.bRegimeAdd) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Режим ведомой  Радиоподавление  (код " + Convert.ToString(TestChannelRead.bRegimeAdd) + ")\n";
                            break;
                    }

                    SetReadText(strTimeCodeAdr, strTypeCmd, strInform);

                    OnReadByte?.Invoke();
                    TestChannelEvent?.Invoke(TestChannelRead);

                    break;
            }



            return iRes;
        }
    }
}
