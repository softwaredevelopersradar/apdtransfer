﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        public delegate void TextMessageJEventHandler(TTextMessage TextMessageJRead);
        public event TextMessageJEventHandler TextMessageJEvent;

        public delegate void ReceptionEventHandler(TAnsReception AnsReceptionRead);
        public event ReceptionEventHandler ReceptionEvent;

        public delegate void CoordJammerEventHandler(TCoordJammer CoordJammerRead);
        public event CoordJammerEventHandler CoordJammerEvent;

        public delegate void DataFWSEventHandler(TDataFWSPartOne DataFWSPartOneRead, TDataFWSPartTwo DataFWSPartTwoRead);
        public event DataFWSEventHandler DataFWSEvent;

        public delegate void DataFWS1EventHandler(TDataFWSPartOne DataFWSPartOneRead);
        public event DataFWS1EventHandler DataFWS1Event;

        public delegate void DataFWS2EventHandler(TDataFWSPartTwo DataFWSPartTwoRead);
        public event DataFWS2EventHandler DataFWS2Event;

        public delegate void DataFHSSEventHandler(TDataFHSSPartOne DataFHSSPartOneRead, TDataFHSSPartTwo DataFHSSPartTwoRead);
        public event DataFHSSEventHandler DataFHSSEvent;

        public delegate void DataFHSS1EventHandler(TDataFHSSPartOne DataFHSSPartOneRead);
        public event DataFHSS1EventHandler DataFHSS1Event;

        public delegate void DataFHSS2EventHandler(TDataFHSSPartTwo DataFHSSPartTwoRead);
        public event DataFHSS2EventHandler DataFHSS2Event;

        public delegate void StateSupressFreqEventHandler(TStateSupressFreq StateSupressFreqRead);
        public event StateSupressFreqEventHandler StateSupressFreqEvent;

        public delegate void ExecuteBearingEventHandler(TExecuteBearing ExecuteBearingRead);
        public event ExecuteBearingEventHandler ExecuteBearingEvent;

        public delegate void SimultanBearingEventHandler(TSimultanBearing SimultanBearingRead);
        public event SimultanBearingEventHandler TSimultanBearingEvent;

        public delegate void TestChannelEventHandler(TTestChannel TestChannelRead);
        public event TestChannelEventHandler TestChannelEvent;
    }
}
