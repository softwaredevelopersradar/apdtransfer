﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        public delegate void OnReadEventHandler();
        public event OnReadEventHandler OnReadByte;

        public delegate void IsWriteEventHandler();
        public event IsWriteEventHandler OnWriteByte;

        public delegate void COMisOpenEventHandler(bool isOpen);
        public event COMisOpenEventHandler COMisOpen;

        private bool _СlaimСheck = false;
        public bool СlaimСheck { get => _СlaimСheck; set => _СlaimСheck = value; }

        private string _cmbNamePort = "COM1";

        public enum WhoIsWho
        {
            CommandCentre = 0,
            Jammer = 1
        }

        private WhoIsWho _whoIswho;
        public WhoIsWho whoIswho { get => _whoIswho; set => _whoIswho = value; }
       

        public void InitRole(WhoIsWho whoIswho)
        {
            _whoIswho = whoIswho;
        }

        public void OpenCOM(byte COMnumber)
        {
            InitSerialPort();
            _cmbNamePort = "COM" + COMnumber.ToString();
            OpenClosePort(_cmbNamePort);
            COMisOpen?.Invoke(true);
        }

        public void CloseCOM()
        {
            OpenClosePort(_cmbNamePort);
            COMisOpen?.Invoke(false);
        }
      
    }
}
