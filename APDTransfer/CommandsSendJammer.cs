﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        public void SendJTextMessage(byte sender, string message)
        {
            // Текстовое сообщение 
            int iAdrSend = sender;
            int iCodeCmd = 3;

            TextMessageSend.Init();

            TextMessageSend.strText = "";
            for (int i = 0; i < 150; i++)
                TextMessageSend.bText[i] = 0;

            TextMessageSend.strText = message;

            int iLenText = 0;
            iLenText = TextMessageSend.strText.Length;

            TextMessageSend.bCountPack = Convert.ToByte(Math.Ceiling(iLenText / 9.0));
            byte[] bSendText = Encoding.Default.GetBytes(TextMessageSend.strText);

            for (int i = 0; i < iLenText; i++)
                TextMessageSend.bText[i] = bSendText[i];


            for (int i = 0; i < iLenText; i++)
            {
                if ((TextMessageSend.bText[i] > 191) & (TextMessageSend.bText[i] <= 239))
                    TextMessageSend.bText[i] = Convert.ToByte((Convert.ToInt32(TextMessageSend.bText[i]) - 64));
                else
                {
                    if ((TextMessageSend.bText[i] > 239) & (TextMessageSend.bText[i] <= 255))
                        TextMessageSend.bText[i] = Convert.ToByte((Convert.ToInt32(TextMessageSend.bText[i]) - 16));
                }

            }

            TextMessageSend.bTempPack = 1;

            while (TextMessageSend.bTempPack <= TextMessageSend.bCountPack)
            {
                SendCommandToAKPP(iCodeCmd, iAdrSend);
                TextMessageSend.bTempPack++;
                OnWriteByte?.Invoke();
            }
        }

        public void SendJReceiptConfirmation(byte sender, byte CodeCdg, byte PackageNumberInCdg, byte ErrorCode)
        {
            // Квитанция подтверждения приема
            int iAdrSend = sender;
            int iCodeCmd = 11;

            AnsReceptionSend.Init();

            AnsReceptionSend.bCmd = CodeCdg;
            AnsReceptionSend.bTempPack = PackageNumberInCdg;
            AnsReceptionSend.bCodeError = ErrorCode;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public void SendJStationCoordinates(byte sender, byte SignLat, byte SignLon,
                                             byte LatitudeDegree, byte LatitudeMinutes, byte LatitudeSeconds,
                                             byte LongitudeDegree, byte LongitudeMinutes, byte LongitudeSeconds)
        {
            // Координаты станции помех
            int iAdrSend = sender;
            int iCodeCmd = 12;

            CoordJammerSend.Init();

            CoordJammerSend.bSignLong = SignLon;

            CoordJammerSend.bLongDegree = LongitudeDegree;
            CoordJammerSend.bLongMinute = LongitudeMinutes;
            CoordJammerSend.bLongSecond = LongitudeSeconds;

            CoordJammerSend.bSignLat = SignLat;

            CoordJammerSend.bLatDegree = LatitudeDegree;
            CoordJammerSend.bLatMinute = LatitudeMinutes;
            CoordJammerSend.bLatSecond = LatitudeSeconds;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }
        public void SendJStationCoordinates(byte sender, TCoordJammer coordJammer)
        {
            // Координаты станции помех
            int iAdrSend = sender;
            int iCodeCmd = 12;

            CoordJammerSend = coordJammer;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public byte ConvertFromSignalWidthToWidthCode(double WidthFWSkHz)
        {
            byte bCodeWidth = 0;

            if (WidthFWSkHz <= 5)
                bCodeWidth = 0;
            if ((WidthFWSkHz > 5) & (WidthFWSkHz <= 12.5))
                bCodeWidth = 1;
            if ((WidthFWSkHz > 12.5) & (WidthFWSkHz <= 25))
                bCodeWidth = 2;
            if ((WidthFWSkHz > 25) & (WidthFWSkHz <= 125))
                bCodeWidth = 3;
            if ((WidthFWSkHz > 125) & (WidthFWSkHz <= 3000))
                bCodeWidth = 4;
            if ((WidthFWSkHz > 3000) & (WidthFWSkHz <= 3500))
                bCodeWidth = 5;
            if ((WidthFWSkHz > 3500) & (WidthFWSkHz <= 8330))
                bCodeWidth = 6;
            if (WidthFWSkHz > 8330)
                bCodeWidth = 7;

            return bCodeWidth;
        }

        public byte ConvertFromStringTypeModeFWStoModeFWS(string TypeModeFWS)
        {
            switch (TypeModeFWS)
            {
                case "АМ":
                    return 0;
                case "ЧМ":
                    return 1;
                case "ЧМ-2":
                    return 2;
                case "НБП":
                    return 3;
                case "ВБП":
                    return 4;
                case "ШПС":
                    return 5;
                case "Несущая":
                    return 6;
                case "Неопр.":
                    return 7;
                case "АМ-2":
                    return 8;
                case "ФМ-2":
                    return 9;
                case "ФМ-4":
                    return 10;
                case "ФМ-8":
                    return 11;
                case "КАМ-16":
                    return 12;
                case "КАМ-32":
                    return 13;
                case "КАМ-64":
                    return 14;
                case "КАМ-128":
                    return 15;
                case "ППРЧ":
                    return 16;
                default:
                    return 7;
            }
        }

        public enum TypeModeFWS
        {
            АМ = 0,
            ЧМ = 1,
            ЧМ2 = 2,
            НБП = 3,
            ВБП = 4,
            ШПС = 5,
            Несущая = 6,
            Неопр = 7,
            АМ2 = 8,
            ФМ2 = 9,
            ФМ4 = 10,
            ФМ8 = 11,
            КАМ16 = 12,
            КАМ32 = 13,
            КАМ64 = 14,
            КАМ128 = 15,
            ППРЧ = 16
        }

        public void SendJFRSInformation(byte sender, TDataFWSPartOne dataFWSPartOneSend, TDataFWSPartTwo dataFWSPartTwoSend)
        {
            // Информация об ИРИ ФЧ
            int iAdrSend = sender;
            int iCodeCmd = 13;

            DataFWSPartOneSend = dataFWSPartOneSend;
            DataFWSPartTwoSend = dataFWSPartTwoSend;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
            SendCommandToAKPP(18, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public byte ConvertFromFHSSWidthToWidthCode(double WidthFHSSkHz)
        {
            byte bCodeWidth = 0;

            if (WidthFHSSkHz <= 5)
                bCodeWidth = 0;
            if ((WidthFHSSkHz > 5) & (WidthFHSSkHz <= 12.5))
                bCodeWidth = 1;
            if ((WidthFHSSkHz > 12.5) & (WidthFHSSkHz <= 25))
                bCodeWidth = 2;
            if ((WidthFHSSkHz > 25) & (WidthFHSSkHz <= 125))
                bCodeWidth = 3;
            if ((WidthFHSSkHz > 125) & (WidthFHSSkHz <= 3000))
                bCodeWidth = 4;
            if ((WidthFHSSkHz > 3000) & (WidthFHSSkHz <= 3500))
                bCodeWidth = 5;
            if ((WidthFHSSkHz > 3500) & (WidthFHSSkHz <= 8330))
                bCodeWidth = 6;
            if (WidthFHSSkHz > 8330)
                bCodeWidth = 7;

            return bCodeWidth;
        }

        public byte ConvertFromFHSSStepToStepCode(double StepFHSSkHz)
        {
            byte bCodeStep = 0;

            if (StepFHSSkHz <= 5)
                bCodeStep = 1;
            if ((StepFHSSkHz > 5) & (StepFHSSkHz <= 12.5))
                bCodeStep = 2;
            if ((StepFHSSkHz > 12.5) & (StepFHSSkHz <= 25))
                bCodeStep = 3;
            if ((StepFHSSkHz > 25) & (StepFHSSkHz <= 125))
                bCodeStep = 4;
            if ((StepFHSSkHz > 125) & (StepFHSSkHz <= 3000))
                bCodeStep = 5;
            if (StepFHSSkHz > 3000)
                bCodeStep = 6;

            return bCodeStep;
        }

        public double ConvertFromFHSSStepCodeToStepFHSSkHz(byte StepCode)
        {
            switch (StepCode)
            {
                case 0:
                    return 5;
                case 1:
                    return 12.5;
                case 2:
                    return 25;
                case 3:
                    return 125;
                case 4:
                    return 3000;
                default:
                    return 12.5;
            }
        }

        public void ConvertFromFHSSDurationmstoCode(float DuratFHSSms, out byte bGroupDuration, out byte bDuration)
        {
            bGroupDuration = 0;
            bDuration = 0;

            if (DuratFHSSms > 10 & DuratFHSSms <= 100)
            {
                bGroupDuration = 1;
                bDuration = Convert.ToByte(DuratFHSSms);
            }
            else
                bDuration = Convert.ToByte(DuratFHSSms * 10);
        }

        public void SendJFHSSInformation(byte sender, TDataFHSSPartOne dataFHSSPartOneSend, TDataFHSSPartTwo dataFHSSPartTwoSend)
        {
            // Информация об ИРИ ППРЧ
            int iAdrSend = sender;
            int iCodeCmd = 14;

            DataFHSSPartOneSend = dataFHSSPartOneSend;
            DataFHSSPartTwoSend = dataFHSSPartTwoSend;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
            SendCommandToAKPP(19, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public enum SighF
        {
            ФЧ = 0,
            ППРЧ = 1
        }

        public void SendJReceiptJammingFrequencies(byte sender, TStateSupressFreq stateSupressFreqSend)
        {
            // Квитанция о подавляемых частотах
            int iAdrSend = sender;
            int iCodeCmd = 15;

            StateSupressFreqSend = stateSupressFreqSend;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public void SendJExBearingAnswer(byte sender, ushort bearing)
        {
            // Ответ на ИП
            int iAdrSend = sender;
            int iCodeCmd = 16;

            ExecuteBearingSend.Init();

            ExecuteBearingSend.wBear = bearing;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public void SendJQuasiBearingAnswer(byte sender,
                                             ushort BearingOwn, ushort BearingLinker,
                                             byte SignLat, byte SignLon,
                                             byte LatitudeDegree, byte LatitudeMinutes, byte LatitudeSeconds,
                                             byte LongitudeDegree, byte LongitudeMinutes, byte LongitudeSeconds)

        {
            // Ответ на КвП
            int iAdrSend = sender;
            int iCodeCmd = 17;

            SimultanBearingSend.Init();

            SimultanBearingSend.wBearMain = BearingOwn;
            SimultanBearingSend.wBearAdd = BearingLinker;

            SimultanBearingSend.bSignLong = SignLon;

            SimultanBearingSend.bLongDegree = LongitudeDegree;
            SimultanBearingSend.bLongMinute = LongitudeMinutes;
            SimultanBearingSend.bLongSecond = LongitudeSeconds;

            SimultanBearingSend.bSignLat = SignLat;

            SimultanBearingSend.bLatDegree = LatitudeDegree;
            SimultanBearingSend.bLatMinute = LatitudeMinutes;
            SimultanBearingSend.bLatSecond = LatitudeSeconds;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }
        public void SendJQuasiBearingAnswer(byte sender, TSimultanBearing simultanBearing)
        {
            // Ответ на КвП
            int iAdrSend = sender;
            int iCodeCmd = 17;

            SimultanBearingSend = simultanBearing;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

        public void SendJChanelTesting(byte sender, byte RxTx, byte ModeOwn, byte ModeLinked , byte[] chMainL, byte[] chAddL)
        {
            // Тестирование канала
            int iAdrSend = sender;
            int iCodeCmd = 20;

            TestChannelSend.Init();

            TestChannelSend.bSignDir = RxTx;
            TestChannelSend.bRegimeMain = ModeOwn;
            TestChannelSend.bRegimeAdd = ModeLinked;
            TestChannelSend.bLetterMain[0] = chMainL[0];
            TestChannelSend.bLetterMain[1] = chMainL[1];
            TestChannelSend.bLetterMain[2] = chMainL[2];
            TestChannelSend.bLetterMain[3] = chMainL[3];
            TestChannelSend.bLetterMain[4] = chMainL[4];
            TestChannelSend.bLetterMain[5] = chMainL[5];
            TestChannelSend.bLetterMain[6] = chMainL[6];

            TestChannelSend.bLetterAdd[0] = chAddL[0];
            TestChannelSend.bLetterAdd[1] = chAddL[1];
            TestChannelSend.bLetterAdd[2] = chAddL[2];
            TestChannelSend.bLetterAdd[3] = chAddL[3];
            TestChannelSend.bLetterAdd[4] = chAddL[4];
            TestChannelSend.bLetterAdd[5] = chAddL[5];
            TestChannelSend.bLetterAdd[6] = chAddL[6];

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }
        public void SendJChanelTesting(byte sender, TTestChannel testChannel)
        {
            // Тестирование канала
            int iAdrSend = sender;
            int iCodeCmd = 20;

            TestChannelSend = testChannel;

            SendCommandToAKPP(iCodeCmd, iAdrSend);
            OnWriteByte?.Invoke();
        }

    }
}
