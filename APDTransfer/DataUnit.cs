﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        #region Commands
        const int REQUEST_DATA = 1;
        const int SYNCHRONIZE = 2;
        const int TEXT_MESSAGE = 3;
        const int REGIME_WORK = 4;
        const int FORBID_RANGE_FREQ = 5;
        const int RECON_SECTOR_RANGE = 6;
        const int SUPRESS_SECTOR_RANGE = 7;
        const int SUPRESS_FWS = 8;
        const int SUPRESS_FHSS = 9;
        const int REQUEST_BEARING = 10;
        const int ANS_RECEPTION = 11;
        const int COORD_JAMMER = 12;
        const int DATA_FWS_PART_ONE = 13;
        const int DATA_FWS_PART_TWO = 18;
        const int DATA_FHSS_PART_ONE = 14;
        const int DATA_FHSS_PART_TWO = 19;
        const int STATE_SUPRESS_FREQ = 15;
        const int EXECUTE_BEARING = 16;
        const int SIMULTAN_BEARING = 17;
        const int TEST_CHANNEL = 20;
        #endregion

        #region Structs
        TRequestData RequestDataSend;
        TRequestData RequestDataRead;

        TSynchronize SynchronizeSend;
        TSynchronize SynchronizeRead;

        TTextMessage TextMessageSend;
        TTextMessage TextMessageRead;

        TRegimeWork RegimeWorkSend;
        TRegimeWork RegimeWorkRead;

        TForbidRangeFreq ForbidRangeFreqSend;
        TForbidRangeFreq ForbidRangeFreqRead;

        TReconSectorRange ReconSectorRangeSend;
        TReconSectorRange ReconSectorRangeRead;

        TSupressSectorRange SupressSectorRangeSend;
        TSupressSectorRange SupressSectorRangeRead;

        TSupressFWS SupressFWSSend;
        TSupressFWS SupressFWSRead;

        TSupressFHSS SupressFHSSSend;
        TSupressFHSS SupressFHSSRead;

        TRequestBearing RequestBearingSend;
        TRequestBearing RequestBearingRead;

        TAnsReception AnsReceptionSend;
        TAnsReception AnsReceptionRead;

        TCoordJammer CoordJammerSend;
        TCoordJammer CoordJammerRead;

        TDataFWSPartOne DataFWSPartOneSend;
        TDataFWSPartOne DataFWSPartOneRead;

        TDataFWSPartTwo DataFWSPartTwoSend;
        TDataFWSPartTwo DataFWSPartTwoRead;

        TDataFHSSPartOne DataFHSSPartOneSend;
        TDataFHSSPartOne DataFHSSPartOneRead;

        TDataFHSSPartTwo DataFHSSPartTwoSend;
        TDataFHSSPartTwo DataFHSSPartTwoRead;

        TStateSupressFreq StateSupressFreqSend;
        TStateSupressFreq StateSupressFreqRead;

        TExecuteBearing ExecuteBearingSend;
        TExecuteBearing ExecuteBearingRead;

        TSimultanBearing SimultanBearingSend;
        TSimultanBearing SimultanBearingRead;

        TTestChannel TestChannelSend;
        TTestChannel TestChannelRead;
        #endregion

        private string strNamePort;

        int iAdrSend;
        int iAdrReceive;
        int iCodeCmd;
        byte[] bBufSend = new byte[12];
        BitArray btaTemp = new BitArray(8);
        BitArray btaResult = new BitArray(8);

        byte[] bBufSave = new byte[256];
        byte[] bBufDecode = new byte[12];
        int iTempLength = 0;

        SerialPort spPortAKPP = new SerialPort();

        private void InitSerialPort()
        {
            spPortAKPP.BaudRate = 9600;
            spPortAKPP.DataBits = 8;
            spPortAKPP.DiscardNull = false;
            spPortAKPP.DtrEnable = true;
            spPortAKPP.RtsEnable = true;
            spPortAKPP.Handshake = Handshake.None;
            spPortAKPP.Parity = Parity.None;
            spPortAKPP.ParityReplace = 63;
            spPortAKPP.PortName = "COM1";
            spPortAKPP.ReadBufferSize = 4096;
            spPortAKPP.ReadTimeout = -1;
            spPortAKPP.ReceivedBytesThreshold = 1;
            spPortAKPP.StopBits = StopBits.One;
            spPortAKPP.WriteBufferSize = 4096;
            spPortAKPP.WriteTimeout = -1;
            //spPortAKPP.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.spPortAKPP_DataReceived);
            spPortAKPP.DataReceived += SpPortAKPP_DataReceived;
        }

        private string GetParamHind(byte bModulation, byte bDeviation, byte bManipulation)
        {
            string strModulation = "";      // модуляция
            string strDeviation = "";       // девиация
            string strManipulation = "";    // манипуляция

            string strHindrance = "";       // все параметры

            switch (bModulation)
            {
                case 1:
                    strModulation = "ЧМ  (код 1)";

                    switch (bDeviation)
                    {
                        case 0:
                            strDeviation = "+/- 1,75 кГц  (код 0)";
                            break;

                        case 1:
                            strDeviation = "+/- 3,5 кГц  (код 1)";
                            break;

                        case 2:
                            strDeviation = "+/- 5,0 кГц  (код 2)";
                            break;

                        case 3:
                            strDeviation = "+/- 10 кГц  (код 3)";
                            break;

                        case 4:
                            strDeviation = "+/- 25 кГц  (код 4)";
                            break;

                        case 5:
                            strDeviation = "+/- 40 кГц  (код 5)";
                            break;

                        case 6:
                            strDeviation = "+/- 100 кГц  (код 6)";
                            break;

                        case 7:
                            strDeviation = "+/- 250 кГц  (код 7)";
                            break;

                        case 8:
                            strDeviation = "+/- 1000 кГц  (код 8)";
                            break;

                        case 9:
                            strDeviation = "+/- 2000 кГц  (код 9)";
                            break;

                        case 10:
                            strDeviation = "+/- 5000 кГц  (код 10)";
                            break;

                        case 11:
                            strDeviation = "+/- 8000 кГц  (код 11)";
                            break;

                        case 12:
                            strDeviation = "+/- 10000 кГц  (код 12)";
                            break;
                    }
                    break;

                case 2:
                    strModulation = "ЧМ-2 (код 2) ";

                    switch (bDeviation)
                    {
                        case 0:
                            strDeviation = "+/- 2 кГц  (код 0)";
                            break;

                        case 1:
                            strDeviation = "+/- 4 кГц  (код 1)";
                            break;

                        case 2:
                            strDeviation = "+/- 6 кГц  (код 2)";
                            break;

                        case 3:
                            strDeviation = "+/- 8 кГц  (код 3)";
                            break;

                        case 4:
                            strDeviation = "+/- 10 кГц  (код 4)";
                            break;

                        case 5:
                            strDeviation = "+/- 12 кГц  (код 5)";
                            break;

                        case 6:
                            strDeviation = "+/- 14 кГц  (код 6)";
                            break;

                        case 7:
                            strDeviation = "+/- 16 кГц  (код 7)";
                            break;

                        case 8:
                            strDeviation = "+/- 18 кГц  (код 8)";
                            break;

                        case 9:
                            strDeviation = "+/- 20 кГц  (код 9)";
                            break;

                        case 10:
                            strDeviation = "+/- 22 кГц  (код 10)";
                            break;

                        case 11:
                            strDeviation = "+/- 24 кГц  (код 11)";
                            break;
                    }

                    switch (bManipulation)
                    {
                        case 0:
                            strManipulation = "62,5 мкс  (код 0)";
                            break;

                        case 1:
                            strManipulation = "125 мкс  (код 1)";
                            break;

                        case 2:
                            strManipulation = "250 мкс  (код 2)";
                            break;

                        case 3:
                            strManipulation = "500 мкс  (код 3)";
                            break;

                        case 4:
                            strManipulation = "1 мс  (код 4)";
                            break;

                        case 5:
                            strManipulation = "2 мс  (код 5)";
                            break;

                        case 6:
                            strManipulation = "4 мс  (код 6)";
                            break;

                        case 7:
                            strManipulation = "8 мс  (код 7)";
                            break;
                    }
                    break;

                case 3:
                    strModulation = "ЧМ-4  (код 3)";

                    switch (bDeviation)
                    {
                        case 0:
                            strDeviation = "+/- 2 кГц  (код 0)";
                            break;

                        case 1:
                            strDeviation = "+/- 4 кГц  (код 1)";
                            break;

                        case 2:
                            strDeviation = "+/- 6 кГц  (код 2)";
                            break;

                        case 3:
                            strDeviation = "+/- 8 кГц  (код 3)";
                            break;

                        case 4:
                            strDeviation = "+/- 10 кГц  (код 4)";
                            break;

                        case 5:
                            strDeviation = "+/- 12 кГц  (код 5)";
                            break;

                        case 6:
                            strDeviation = "+/- 14 кГц  (код 6)";
                            break;

                        case 7:
                            strDeviation = "+/- 16 кГц  (код 7)";
                            break;

                        case 8:
                            strDeviation = "+/- 18 кГц  (код 8)";
                            break;

                        case 9:
                            strDeviation = "+/- 20 кГц  (код 9)";
                            break;

                        case 10:
                            strDeviation = "+/- 22 кГц  (код 10)";
                            break;

                        case 11:
                            strDeviation = "+/- 24 кГц  (код 11)";
                            break;
                    }

                    switch (bManipulation)
                    {
                        case 0:
                            strManipulation = "62,5 мкс  (код 0)";
                            break;

                        case 1:
                            strManipulation = "125 мкс  (код 1)";
                            break;

                        case 2:
                            strManipulation = "250 мкс  (код 2)";
                            break;

                        case 3:
                            strManipulation = "500 мкс  (код 3)";
                            break;

                        case 4:
                            strManipulation = "1 мс  (код 4)";
                            break;

                        case 5:
                            strManipulation = "2 мс  (код 5)";
                            break;

                        case 6:
                            strManipulation = "4 мс  (код 6)";
                            break;

                        case 7:
                            strManipulation = "8 мс  (код 7)";
                            break;
                    }
                    break;

                case 6:
                    strModulation = "ЧМШ  (код 6)";

                    switch (bDeviation)
                    {
                        case 0:
                            strDeviation = "+/- 1,75 кГц  (код 0)";
                            break;

                        case 1:
                            strDeviation = "+/- 3,5 кГц  (код 1)";
                            break;

                        case 2:
                            strDeviation = "+/- 5,0 кГц  (код 2)";
                            break;

                        case 3:
                            strDeviation = "+/- 10 кГц  (код 3)";
                            break;

                        case 4:
                            strDeviation = "+/- 25 кГц  (код 4)";
                            break;

                        case 5:
                            strDeviation = "+/- 50 кГц  (код 5)";
                            break;

                        case 6:
                            strDeviation = "+/- 100 кГц  (код 6)";
                            break;

                        case 7:
                            strDeviation = "+/- 500 кГц  (код 7)";
                            break;

                        case 8:
                            strDeviation = "+/- 1000 кГц  (код 8)";
                            break;
                    }
                    break;

                case 7:
                    strModulation = "ФМн (код 7)";

                    switch (bDeviation)
                    {
                        case 0:
                            strManipulation = "0,2 мкс  (код 0)";
                            break;

                        case 1:
                            strManipulation = "0,5 мкс  (код 1)";
                            break;

                        case 2:
                            strManipulation = "1 мкс  (код 2)";
                            break;

                        case 3:
                            strManipulation = "5 мкс  (код 3)";
                            break;

                        case 4:
                            strManipulation = "10 мкс  (код 4)";
                            break;

                        case 5:
                            strManipulation = "20 мкс  (код 5)";
                            break;

                        case 6:
                            strManipulation = "40 мкс  (код 6)";
                            break;

                        case 7:
                            strManipulation = "80 мкс  (код 7)";
                            break;

                        case 8:
                            strManipulation = "400 мкс  (код 8)";
                            break;
                    }
                    break;

                case 16:
                    strModulation = "ЗШ 2,5 МГц  (код 16)";
                    break;
            }

            strHindrance = strModulation + " " + strDeviation + " " + strManipulation;
            return strHindrance;
        }

        private void OpenClosePort(string cmbNamePort)
        {
            strNamePort = cmbNamePort;

            if (spPortAKPP.IsOpen)
            {
                try
                {
                    try
                    {
                        spPortAKPP.DiscardInBuffer();
                    }
                    catch (System.Exception ex)
                    {
                        //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);
                    }

                    try
                    {
                        spPortAKPP.DiscardOutBuffer();
                    }
                    catch (System.Exception ex)
                    {
                       // MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);
                    }

                    spPortAKPP.Close();
                }
                catch (System.Exception ex)
                {
                    //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);
                }

                //tmStatePort.Enabled = false;
                //ViewState(spPortAKPP.IsOpen, shEnablPort);
                //ViewState(false, shDSR);
                //ViewState(false, shCTS);
                //ViewState(false, shRTS);
                //ViewState(false, shDTR);
                //ViewState(false, shRI);
            }
            else
            {
                if (strNamePort != "")
                {
                    spPortAKPP.PortName = strNamePort;
                    spPortAKPP.BaudRate = 4800;
                    spPortAKPP.Parity = System.IO.Ports.Parity.None;
                    spPortAKPP.DataBits = 8;
                    spPortAKPP.StopBits = System.IO.Ports.StopBits.One;

                    try
                    {
                        spPortAKPP.Open();
                        spPortAKPP.DataReceived += SpPortAKPP_DataReceived;
                    }

                    catch (System.Exception ex)
                    {
                        //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);

                    }
                    //tmStatePort.Enabled = true;
                }
            }
        }

        private void SpPortAKPP_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] bBufRead;
            bBufRead = new byte[256];
            int iReadByte = -1;

            byte bCodeRead = 0;

            string strInform = "";

            try
            {
                iReadByte = spPortAKPP.Read(bBufRead, 0, 256);

                //if (chBytes.Checked)
                if (true)
                {
                    string strResHex = string.Empty;
                    string strResBin = string.Empty;
                    int iDecValue = 0;

                    for (int i = 0; i < iReadByte; i++)
                    {
                        iDecValue = 0;
                        iDecValue = bBufRead[i];
                        strResHex = iDecValue.ToString("X2");
                        strResBin = Convert.ToString(iDecValue, 2);
                        while (strResBin.Length < 8)
                        {
                            strResBin = "0" + strResBin;
                        }

                        if (i <= 8)
                            strInform = strInform + " " + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                        else
                            strInform = strInform + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                    }

                    SetReadByte(strInform, iReadByte);
                }


                for (int i = iTempLength; i < iTempLength + iReadByte; i++)
                    bBufSave[i] = bBufRead[i - iTempLength];

                iTempLength += iReadByte;

                while (iTempLength >= 12)
                {
                    for (int i = 0; i < 12; i++)
                        bBufDecode[i] = 0;

                    for (int i = 0; i < 12; i++)
                        bBufDecode[i] = bBufSave[i];

                    try
                    {
                        bCodeRead = bBufDecode[0];
                    }
                    catch (System.Exception ex)
                    {
                        bCodeRead = 0;
                        //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);
                    }


                    //if (rbAKPP.Checked)
                    //Если мы АКПП
                    if(_whoIswho == WhoIsWho.CommandCentre)
                    {
                        ReadCommandFromJammer(bCodeRead, bBufDecode);
                    }
                    
                    //if (rbJammer.Checked)
                    //Если мы джамер
                    if(_whoIswho == WhoIsWho.Jammer)
                    {
                        ReadCommandFromAKKP(bCodeRead, bBufDecode);
                    }

                    for (int i = 0; i < iTempLength; i++)
                        bBufSave[i] = bBufSave[i + 12];

                    iTempLength = iTempLength - 12;

                } // while (iTempLength > 12)
            }
            catch (System.Exception ex)
            {

                //MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK);
            }

        }
    }
}