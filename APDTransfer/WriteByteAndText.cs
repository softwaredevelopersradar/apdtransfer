﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        delegate void SetTextCallbackWrite(string strTimeCodeAdr, string strTypeCmd, string strInform);

        delegate void SetByteCallbackWrite(string strBytes, int iCountByte);

        private void SetWriteText(string strTimeCodeAdr, string strTypeCmd, string strInform)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            //if (this.rtbLogMessage.InvokeRequired)
            //{
            //    SetTextCallbackWrite d = new SetTextCallbackWrite(SetWriteText);
            //    this.BeginInvoke(d, new object[] { strTimeCodeAdr, strTypeCmd, strInform });
            //}
            //else
            //{
            //    rtbLogMessage.AppendText("\n");
            //    rtbLogMessage.SelectionColor = Color.Blue;
            //    rtbLogMessage.AppendText(strTimeCodeAdr);
            //    rtbLogMessage.SelectionColor = Color.Blue;
            //    rtbLogMessage.AppendText(strTypeCmd);
            //    rtbLogMessage.SelectionColor = Color.Black;
            //    rtbLogMessage.AppendText(strInform);
            //    rtbLogMessage.SelectionStart = rtbLogMessage.TextLength;
            //    rtbLogMessage.ScrollToCaret();
            //}
        }

        private void SetWriteByte(string strBytes, int iCountByte)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            //if (this.rtbLogMessage.InvokeRequired)
            //{
            //    SetByteCallbackWrite d = new SetByteCallbackWrite(SetWriteByte);
            //    this.BeginInvoke(d, new object[] { strBytes, iCountByte });
            //}
            //else
            //{
            //    rtbLogMessage.AppendText("\n");
            //    rtbLogMessage.SelectionColor = Color.Blue;
            //    rtbLogMessage.AppendText("Отправлено  " + Convert.ToString(iCountByte) + "  байт \n");
            //    rtbLogMessage.SelectionColor = Color.Indigo;
            //    rtbLogMessage.AppendText(strBytes);
            //    rtbLogMessage.SelectionStart = rtbLogMessage.TextLength;
            //    rtbLogMessage.ScrollToCaret();
            //}
        }

    }
}
