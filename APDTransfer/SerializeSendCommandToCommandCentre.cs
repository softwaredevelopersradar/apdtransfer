﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APDTransfer
{
    public partial class APDTransferClass
    {
        private int SendCommandToAKPP(int iNumCmd, int iAdrSender)
        {
            int iRes = -1;

            for (int i = 0; i < 12; i++)
                bBufSend[i] = 0;

            byte[] bTemp = new byte[1];
            byte[] bByteBit = new byte[1];

            byte[] intBytes;
            byte[] result;

            string strTimeCodeAdr = "";
            string strTypeCmd = "";
            string strInform = "";

            switch (iNumCmd)
            {

                //--------------------------------                
                case TEXT_MESSAGE:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);

                    // адрес отправителя     
                    btaResult.SetAll(false);

                    bTemp = new byte[1];
                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];


                    // всего пакетов
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = TextMessageSend.bCountPack;
                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];

                    // номер текущего пакета
                    bBufSend[2] = TextMessageSend.bTempPack;

                    // текст
                    string strTextTemp = "";

                    for (int i = 0; i < 9; i++)
                    {
                        bBufSend[i + 3] = TextMessageSend.bText[(TextMessageSend.bTempPack - 1) * 9 + i];
                    }

                    byte[] bTextTemp = new byte[9];
                    for (int i = 0; i < 9; i++)
                    {
                        bTextTemp[i] = bBufSend[i + 3];
                        if ((bTextTemp[i] > 127) & (bTextTemp[i] <= 175))
                            bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 64));
                        else
                        {
                            if ((bTextTemp[i] > 223) & (bTextTemp[i] < 241))
                                bTextTemp[i] = Convert.ToByte((Convert.ToInt32(bTextTemp[i]) + 16));
                        }
                    }
                    strTextTemp = Encoding.Default.GetString(bTextTemp) + "\n";

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Текстовое сообщение\n";

                    strInform = strInform + "Всего пакетов " + Convert.ToString(TextMessageSend.bCountPack + "\n");
                    strInform = strInform + "Номер пакета " + Convert.ToString(TextMessageSend.bTempPack + "\n");
                    strInform = strInform + strTextTemp;

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case ANS_RECEPTION:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // код ошибки          
                    bBufSend[2] = Convert.ToByte(AnsReceptionSend.bCodeError);     //* 2 *//

                    // шифр принятой кодограммы    
                    bBufSend[3] = Convert.ToByte(AnsReceptionSend.bCmd);           //* 3 *//

                    // номер пакета (строки, ИРИ) 
                    bBufSend[4] = Convert.ToByte(AnsReceptionSend.bTempPack);      //* 4 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Квитанция подтверждения\n";

                    strInform = strInform + "Код ошибки " + Convert.ToString(AnsReceptionSend.bCodeError + "\n");
                    strInform = strInform + "Шифр кдг " + Convert.ToString(AnsReceptionSend.bCmd + "\n");
                    strInform = strInform + "Номер пакета (строки, ИРИ) " + Convert.ToString(AnsReceptionSend.bTempPack + "\n");

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;


                //--------------------------------                                
                case COORD_JAMMER:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // знак, градусы долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;

                    btaResult[0] = Convert.ToBoolean(CoordJammerSend.bSignLong);

                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                     //* 2 *//

                    // градусы долготы, минуты долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);
                    btaResult[0] = btaTemp[7];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLongMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[2] = btaTemp[0];
                    btaResult[3] = btaTemp[1];
                    btaResult[4] = btaTemp[2];
                    btaResult[5] = btaTemp[3];
                    btaResult[6] = btaTemp[4];
                    btaResult[7] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // секунды долготы    
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLongSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//


                    // знак, градусы широты 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    btaResult[0] = Convert.ToBoolean(CoordJammerSend.bSignLat);

                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLatDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//

                    // минуты широты  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLatMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // секунды широты   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLatSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//

                    // сотые доли секунд долготы
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLongPartSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[8] = bByteBit[0];                                     //* 8 *//

                    // сотые доли секунд широты  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(CoordJammerSend.bLatPartSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Координаты СП\n";

                    strInform = strInform + Convert.ToString(CoordJammerSend.bLongDegree) + " " + Convert.ToString(CoordJammerSend.bLongMinute) +
                                             " " + Convert.ToString(CoordJammerSend.bLongSecond) + "," + Convert.ToString(CoordJammerSend.bLongPartSecond);
                    if (CoordJammerSend.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(CoordJammerSend.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(CoordJammerSend.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(CoordJammerSend.bLatDegree) + " " + Convert.ToString(CoordJammerSend.bLatMinute) +
                                             " " + Convert.ToString(CoordJammerSend.bLatSecond) + "," + Convert.ToString(CoordJammerSend.bLatPartSecond);
                    if (CoordJammerSend.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(CoordJammerSend.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(CoordJammerSend.bSignLat) + ")\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case DATA_FWS_PART_ONE:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // номер ИРИ
                    bBufSend[2] = Convert.ToByte(DataFWSPartOneSend.bTempSource);  //* 2 *//

                    // частота                   
                    intBytes = BitConverter.GetBytes(DataFWSPartOneSend.iFreq);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                       //* 3 *//

                    bBufSend[4] = result[2];                                       //* 4 *//

                    bBufSend[5] = result[1];                                       //* 5 *//

                    // код вида модуляции
                    bBufSend[6] = Convert.ToByte(DataFWSPartOneSend.bCodeType);    //* 6 *//

                    // код ширины
                    bBufSend[7] = Convert.ToByte(DataFWSPartOneSend.bCodeWidth);   //* 7 *//

                    // часы
                    bBufSend[8] = Convert.ToByte(DataFWSPartOneSend.bHour);        //* 8 *//

                    // минуты
                    bBufSend[9] = Convert.ToByte(DataFWSPartOneSend.bMin);         //* 9 *//

                    // секунды
                    bBufSend[10] = Convert.ToByte(DataFWSPartOneSend.bSec);        //* 10 *//

                    // код скорости ТЛГ
                    bBufSend[11] = Convert.ToByte(DataFWSPartOneSend.bCodeRate);   //* 11 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ФЧ 1\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFWSPartOneSend.bTempSource) + "\n";
                    strInform = strInform + "Частота " + Convert.ToString(DataFWSPartOneSend.iFreq) + "\n";

                    switch (DataFWSPartOneSend.bCodeType)
                    {
                        case 1:
                            strInform = strInform + "Модуляция АМ (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Модуляция ЧМ (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Модуляция ЧМ-2 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Модуляция НБП (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Модуляция ВБП (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Модуляция ШПС (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Модуляция нес (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Модуляция неопр (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 9:
                            strInform = strInform + "Модуляция АМ-2 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 10:
                            strInform = strInform + "Модуляция ФМ-2 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 11:
                            strInform = strInform + "Модуляция ФМ-4 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 12:
                            strInform = strInform + "Модуляция ФМ-8 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 13:
                            strInform = strInform + "Модуляция КАМ-16 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 14:
                            strInform = strInform + "Модуляция КАМ-32 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 15:
                            strInform = strInform + "Модуляция КАМ-64 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 16:
                            strInform = strInform + "Модуляция КАМ-128 (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;

                        case 17:
                            strInform = strInform + "Модуляция ППРЧ (код " + Convert.ToString(DataFWSPartOneSend.bCodeType) + ")\n";
                            break;
                    }

                    switch (DataFWSPartOneSend.bCodeWidth)
                    {
                        case 1:
                            strInform = strInform + "Ширина < 5 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Ширина 5-12,5 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Ширина 12,5-25 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Ширина 25-25 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Ширина 125-3000 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Ширина 3000-3500 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Ширина 3500-8330 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Ширина > 8330 кГц (код " + Convert.ToString(DataFWSPartOneSend.bCodeWidth) + ")\n";
                            break;
                    }

                    strInform = strInform + "Время  " + Convert.ToString(DataFWSPartOneSend.bHour) + ":"
                                             + Convert.ToString(DataFWSPartOneSend.bMin) + ":" + Convert.ToString(DataFWSPartOneSend.bSec) + "\n";

                    switch (DataFWSPartOneSend.bCodeRate)
                    {
                        case 1:
                            strInform = strInform + "Скорость < 10 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Скорость 10-20 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Скорость 20-50 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Скорость 50-100 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 5:
                            strInform = strInform + "Скорость 100-200 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 6:
                            strInform = strInform + "Скорость 200-500 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 7:
                            strInform = strInform + "Скорость 500-1 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 8:
                            strInform = strInform + "Скорость 1 000-2 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 9:
                            strInform = strInform + "Скорость 2 000-5 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 10:
                            strInform = strInform + "Скорость 5 000-10 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 11:
                            strInform = strInform + "Скорость 10 000-20 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 12:
                            strInform = strInform + "Скорость 20 000-50 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 13:
                            strInform = strInform + "Скорость 50 000-100 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 14:
                            strInform = strInform + "Скорость 100 000-200 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 15:
                            strInform = strInform + "Скорость 200 000-500 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 16:
                            strInform = strInform + "Скорость 500 000-1 000 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 17:
                            strInform = strInform + "Скорость 1 000 000-2 000 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 18:
                            strInform = strInform + "Скорость 2 000 000-5 000 000 бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                        case 19:
                            strInform = strInform + "Скорость > 5 000 000  бод (код " + Convert.ToString(DataFWSPartOneSend.bCodeRate) + ")\n";
                            break;
                    }

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------
                case DATA_FWS_PART_TWO:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // номер ИРИ
                    bBufSend[2] = Convert.ToByte(DataFWSPartTwoSend.bTempSource);  //* 2 *//

                    // знак, градусы долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;

                    btaResult[0] = Convert.ToBoolean(DataFWSPartTwoSend.bSignLong);

                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // градусы долготы, минуты долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);
                    btaResult[0] = btaTemp[7];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLongMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[2] = btaTemp[0];
                    btaResult[3] = btaTemp[1];
                    btaResult[4] = btaTemp[2];
                    btaResult[5] = btaTemp[3];
                    btaResult[6] = btaTemp[4];
                    btaResult[7] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//

                    // секунды долготы   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLongSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];
                    btaResult[7] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//


                    // знак, градусы широты 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    btaResult[0] = Convert.ToBoolean(DataFWSPartTwoSend.bSignLat);

                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLatDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // минуты широты  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLatMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//

                    // секунды широты  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFWSPartTwoSend.bLatSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[8] = bByteBit[0];                                     //* 8 *//

                    // пеленг от ведущей 
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFWSPartTwoSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//

                    // пеленг от ведущей, пеленг от ведомой
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFWSPartTwoSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    intBytes = BitConverter.GetBytes(DataFWSPartTwoSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];
                    btaResult[6] = btaTemp[13];
                    btaResult[7] = btaTemp[14];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[10] = bByteBit[0];                                     //* 10 *//


                    // пеленг от ведомой   
                    btaResult.SetAll(false);
                    btaTemp.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFWSPartTwoSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[11] = bByteBit[0];                                     //* 11 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ФЧ 2\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFWSPartTwoSend.bTempSource) + "\n";

                    strInform = strInform + Convert.ToString(DataFWSPartTwoSend.bLongDegree) + " " + Convert.ToString(DataFWSPartTwoSend.bLongMinute) +
                                             " " + Convert.ToString(DataFWSPartTwoSend.bLongSecond);
                    if (DataFWSPartTwoSend.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(DataFWSPartTwoSend.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(DataFWSPartTwoSend.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(DataFWSPartTwoSend.bLatDegree) + " " + Convert.ToString(DataFWSPartTwoSend.bLatMinute) +
                                             " " + Convert.ToString(DataFWSPartTwoSend.bLatSecond);
                    if (DataFWSPartTwoSend.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(DataFWSPartTwoSend.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(DataFWSPartTwoSend.bSignLat) + ")\n";

                    strInform = strInform + "Пеленг от ведущей " + Convert.ToString(DataFWSPartTwoSend.wBearMain) + "\n";

                    strInform = strInform + "Пеленг от ведомой " + Convert.ToString(DataFWSPartTwoSend.wBearAdd) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;


                //--------------------------------                                
                case DATA_FHSS_PART_ONE:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // номер ИРИ
                    bBufSend[2] = Convert.ToByte(DataFHSSPartOneSend.bTempSource);  //* 2 *//

                    // частота минимальная                    
                    intBytes = BitConverter.GetBytes(DataFHSSPartOneSend.iFreqBegin);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[3] = result[3];                                       //* 3 *//

                    bBufSend[4] = result[2];                                       //* 4 *//

                    bBufSend[5] = result[1];                                       //* 5 *//

                    // частота максимальная    
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFHSSPartOneSend.iFreqEnd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    bBufSend[6] = result[3];                                       //* 6 *//

                    bBufSend[7] = result[2];                                       //* 7 *//

                    bBufSend[8] = result[1];                                       //* 8 *//

                    // часы, минуты   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bHour);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bMin);

                    btaTemp = new BitArray(bTemp);

                    btaResult[6] = btaTemp[0];
                    btaResult[7] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//

                    // минуты, секунды   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bMin);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[2];
                    btaResult[1] = btaTemp[3];
                    btaResult[2] = btaTemp[4];
                    btaResult[3] = btaTemp[5];


                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bSec);

                    btaTemp = new BitArray(bTemp);

                    btaResult[4] = btaTemp[0];
                    btaResult[5] = btaTemp[1];
                    btaResult[6] = btaTemp[2];
                    btaResult[7] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[10] = bByteBit[0];                                     //* 10 *//

                    // секунды, код шага сетки, код шага ширины    
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bSec);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[4];
                    btaResult[1] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bCodeStep);

                    btaTemp = new BitArray(bTemp);

                    btaResult[2] = btaTemp[0];
                    btaResult[3] = btaTemp[1];
                    btaResult[4] = btaTemp[2];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartOneSend.bCodeWidth);

                    btaTemp = new BitArray(bTemp);

                    btaResult[5] = btaTemp[0];
                    btaResult[6] = btaTemp[1];
                    btaResult[7] = btaTemp[2];


                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[11] = bByteBit[0];                                     //* 11 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ППРЧ 1\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFHSSPartOneSend.bTempSource) + "\n";
                    strInform = strInform + "Частота мин." + Convert.ToString(DataFHSSPartOneSend.iFreqBegin) + "\n";
                    strInform = strInform + "Частота макс." + Convert.ToString(DataFHSSPartOneSend.iFreqEnd) + "\n";
                    strInform = strInform + "Время  " + Convert.ToString(DataFHSSPartOneSend.bHour) + ":"
                                             + Convert.ToString(DataFHSSPartOneSend.bMin) + ":" + Convert.ToString(DataFHSSPartOneSend.bSec) + "\n";

                    switch (DataFHSSPartOneSend.bCodeStep)
                    {
                        case 1:
                            strInform = strInform + "Шаг < 5 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Шаг 2-12,5 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                        case 3:
                            strInform = strInform + "Шаг 12,5-25 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                        case 4:
                            strInform = strInform + "Шаг 25-125 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                        case 5:
                            strInform = strInform + "Шаг 125-3000 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                        case 6:
                            strInform = strInform + "Шаг > 3000 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeStep) + ")\n";
                            break;
                    }

                    switch (DataFHSSPartOneSend.bCodeWidth)
                    {
                        case 1:
                            strInform = strInform + "Ширина < 5 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Ширина 5-12,5 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Ширина 12,5-25 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Ширина 25-25 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Ширина 125-3000 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Ширина 3000-3500 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Ширина 3500-8330 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Ширина > 8330 кГц (код " + Convert.ToString(DataFHSSPartOneSend.bCodeWidth) + ")\n";
                            break;
                    }

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case DATA_FHSS_PART_TWO:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // номер ИРИ
                    btaResult.SetAll(false);

                    bBufSend[2] = Convert.ToByte(DataFHSSPartTwoSend.bTempSource); //* 2 *//

                    // длительность, пеленг от ведущей 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bGroupDuration);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bDuration);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // пеленг от ведущей   
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFHSSPartTwoSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//

                    // пеленг от ведущей, пеленг от ведомой 
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFHSSPartTwoSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    intBytes = BitConverter.GetBytes(DataFHSSPartTwoSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];
                    btaResult[6] = btaTemp[13];
                    btaResult[7] = btaTemp[14];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//


                    // пеленг от ведомой,  знак, градусы долготы
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(DataFHSSPartTwoSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bSignLong);

                    btaTemp = new BitArray(bTemp);

                    btaResult[2] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[3] = btaTemp[0];
                    btaResult[4] = btaTemp[1];
                    btaResult[5] = btaTemp[2];
                    btaResult[6] = btaTemp[3];
                    btaResult[7] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // градусы долготы, минуты долготы
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[5];
                    btaResult[1] = btaTemp[6];
                    btaResult[2] = btaTemp[7];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLongMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[3] = btaTemp[0];
                    btaResult[4] = btaTemp[1];
                    btaResult[5] = btaTemp[2];
                    btaResult[6] = btaTemp[3];
                    btaResult[7] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//

                    // минуты долготы, секунды долготы 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLongMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLongSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bSignLat);

                    btaTemp = new BitArray(bTemp);

                    btaResult[7] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[8] = bByteBit[0];                                     //* 8 *//

                    // градусы широты, минуты широты 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLatDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLatMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[7] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//

                    // минуты широты, секунды широты
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLatMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[1];
                    btaResult[1] = btaTemp[2];
                    btaResult[2] = btaTemp[3];
                    btaResult[3] = btaTemp[4];
                    btaResult[4] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLatSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[5] = btaTemp[0];
                    btaResult[6] = btaTemp[1];
                    btaResult[7] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[10] = bByteBit[0];                                     //* 10 *//

                    // секунды широты, код вида модуляции 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bLatSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[3];
                    btaResult[1] = btaTemp[4];
                    btaResult[2] = btaTemp[5];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(DataFHSSPartTwoSend.bCodeType);

                    btaTemp = new BitArray(bTemp);

                    btaResult[3] = btaTemp[0];
                    btaResult[4] = btaTemp[1];
                    btaResult[5] = btaTemp[2];
                    btaResult[6] = btaTemp[3];
                    btaResult[7] = btaTemp[4];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[11] = bByteBit[0];                                     //* 11 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения об ИРИ ППРЧ 2\n";

                    strInform = strInform + "Номер ИРИ " + Convert.ToString(DataFHSSPartTwoSend.bTempSource) + "\n";

                    if (DataFHSSPartTwoSend.bGroupDuration == 0)
                        strInform = strInform + "Длительность " + Convert.ToString(Convert.ToDouble(DataFHSSPartTwoSend.bDuration) / 10.0);
                    else
                        strInform = strInform + "Длительность " + Convert.ToString(DataFHSSPartTwoSend.bDuration);

                    strInform = strInform + " группа " + Convert.ToString(DataFHSSPartTwoSend.bGroupDuration) + "\n";

                    strInform = strInform + Convert.ToString(DataFHSSPartTwoSend.bLongDegree) + " " + Convert.ToString(DataFHSSPartTwoSend.bLongMinute) +
                                             " " + Convert.ToString(DataFHSSPartTwoSend.bLongSecond);
                    if (DataFHSSPartTwoSend.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(DataFHSSPartTwoSend.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(DataFHSSPartTwoSend.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(DataFHSSPartTwoSend.bLatDegree) + " " + Convert.ToString(DataFHSSPartTwoSend.bLatMinute) +
                                             " " + Convert.ToString(DataFHSSPartTwoSend.bLatSecond);
                    if (DataFHSSPartTwoSend.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(DataFHSSPartTwoSend.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(DataFHSSPartTwoSend.bSignLat) + ")\n";

                    strInform = strInform + "Пеленг от ведущей " + Convert.ToString(DataFHSSPartTwoSend.wBearMain) + "\n";

                    strInform = strInform + "Пеленг от ведомой " + Convert.ToString(DataFHSSPartTwoSend.wBearAdd) + "\n";

                    switch (DataFHSSPartTwoSend.bCodeType)
                    {
                        case 1:
                            strInform = strInform + "Модуляция АМ (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 2:
                            strInform = strInform + "Модуляция ЧМ (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 3:
                            strInform = strInform + "Модуляция ЧМ-2 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 4:
                            strInform = strInform + "Модуляция НБП (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 5:
                            strInform = strInform + "Модуляция ВБП (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 6:
                            strInform = strInform + "Модуляция ШПС (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 7:
                            strInform = strInform + "Модуляция нес (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 8:
                            strInform = strInform + "Модуляция неопр (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 9:
                            strInform = strInform + "Модуляция АМ-2 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 10:
                            strInform = strInform + "Модуляция ФМ-2 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 11:
                            strInform = strInform + "Модуляция ФМ-4 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 12:
                            strInform = strInform + "Модуляция ФМ-8 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 13:
                            strInform = strInform + "Модуляция КАМ-16 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 14:
                            strInform = strInform + "Модуляция КАМ-32 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 15:
                            strInform = strInform + "Модуляция КАМ-64 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 16:
                            strInform = strInform + "Модуляция КАМ-128 (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;

                        case 17:
                            strInform = strInform + "Модуляция ППРЧ (код " + Convert.ToString(DataFHSSPartTwoSend.bCodeType) + ")\n";
                            break;
                    }

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case STATE_SUPRESS_FREQ:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя                    
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // инф-ция о ФЧ 1, инф-ция о ФЧ 2 
                    btaResult.SetAll(false);

                    btaResult[0] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[0]);
                    btaResult[1] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[0]);
                    btaResult[2] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[0]);

                    btaResult[3] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[1]);
                    btaResult[4] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[1]);
                    btaResult[5] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[1]);

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                     //* 2 *//


                    // инф-ция о ФЧ 3, инф-ция о ФЧ 4 
                    btaResult.SetAll(false);

                    btaResult[0] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[2]);
                    btaResult[1] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[2]);
                    btaResult[2] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[2]);

                    btaResult[3] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[3]);
                    btaResult[4] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[3]);
                    btaResult[5] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[3]);

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // инф-ция о ФЧ 5, инф-ция о ФЧ 6 
                    btaResult.SetAll(false);

                    btaResult[0] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[4]);
                    btaResult[1] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[4]);
                    btaResult[2] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[4]);

                    btaResult[3] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[5]);
                    btaResult[4] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[5]);
                    btaResult[5] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[5]);

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//

                    // инф-ция о ФЧ 7, инф-ция о ФЧ 8
                    btaResult.SetAll(false);

                    btaResult[0] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[6]);
                    btaResult[1] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[6]);
                    btaResult[2] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[6]);

                    btaResult[3] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[7]);
                    btaResult[4] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[7]);
                    btaResult[5] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[7]);

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//


                    // инф-ция о ФЧ 9, инф-ция о ФЧ 10 
                    btaResult.SetAll(false);

                    btaResult[0] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[8]);
                    btaResult[1] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[8]);
                    btaResult[2] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[8]);

                    btaResult[3] = Convert.ToBoolean(StateSupressFreqSend.bStateWork[9]);
                    btaResult[4] = Convert.ToBoolean(StateSupressFreqSend.bStateLongTerm[9]);
                    btaResult[5] = Convert.ToBoolean(StateSupressFreqSend.bStateSupress[9]);

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // номер поддиапазона
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(StateSupressFreqSend.bTempSubRange);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//

                    // признак ППРЧ   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(StateSupressFreqSend.bSighF);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[8] = bByteBit[0];                                     //* 8 *//

                    // работа ППРЧ   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(StateSupressFreqSend.bStateFHSS);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Сведения о подавляемых ИРИ\n";

                    strInform = strInform + "Номер п/диапазона " + Convert.ToString(StateSupressFreqSend.bTempSubRange) + "\n";

                    strInform = strInform + "ФЧ 1   " + Convert.ToString(StateSupressFreqSend.bStateWork[0]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[0]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[0] + "\n");
                    strInform = strInform + "ФЧ 2   " + Convert.ToString(StateSupressFreqSend.bStateWork[1]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[1]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[1] + "\n");
                    strInform = strInform + "ФЧ 3   " + Convert.ToString(StateSupressFreqSend.bStateWork[2]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[2]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[2] + "\n");
                    strInform = strInform + "ФЧ 4   " + Convert.ToString(StateSupressFreqSend.bStateWork[3]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[3]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[3] + "\n");
                    strInform = strInform + "ФЧ 5   " + Convert.ToString(StateSupressFreqSend.bStateWork[4]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[4]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[4] + "\n");
                    strInform = strInform + "ФЧ 6   " + Convert.ToString(StateSupressFreqSend.bStateWork[5]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[5]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[5] + "\n");
                    strInform = strInform + "ФЧ 7   " + Convert.ToString(StateSupressFreqSend.bStateWork[6]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[6]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[6] + "\n");
                    strInform = strInform + "ФЧ 8   " + Convert.ToString(StateSupressFreqSend.bStateWork[7]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[7]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[7] + "\n");
                    strInform = strInform + "ФЧ 9   " + Convert.ToString(StateSupressFreqSend.bStateWork[8]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[8]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[8] + "\n");
                    strInform = strInform + "ФЧ 10   " + Convert.ToString(StateSupressFreqSend.bStateWork[9]) + "  " + Convert.ToString(StateSupressFreqSend.bStateLongTerm[9]) + "  "
                                            + Convert.ToString(StateSupressFreqSend.bStateSupress[9] + "\n");

                    strInform = strInform + "Признак ППРЧ " + Convert.ToString(StateSupressFreqSend.bSighF) + "\n";
                    strInform = strInform + "Состояние ППРЧ " + Convert.ToString(StateSupressFreqSend.bStateFHSS) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case EXECUTE_BEARING:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // пеленг  
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(ExecuteBearingSend.wBear);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                     //* 2 *//

                    // пеленг  
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(ExecuteBearingSend.wBear);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Исполнительное пеленгование\n";

                    strInform = strInform + "Пеленг " + Convert.ToString(ExecuteBearingSend.wBear) + "\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case SIMULTAN_BEARING:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//                    

                    // адрес отправителя  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // пеленг от ведущей   
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SimultanBearingSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[8];
                    btaResult[1] = btaTemp[9];
                    btaResult[2] = btaTemp[10];
                    btaResult[3] = btaTemp[11];
                    btaResult[4] = btaTemp[12];
                    btaResult[5] = btaTemp[13];
                    btaResult[6] = btaTemp[14];
                    btaResult[7] = btaTemp[15];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                     //* 2 *//

                    // пеленг от ведущей, пеленг от ведомой     
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SimultanBearingSend.wBearMain);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[0];

                    intBytes = BitConverter.GetBytes(SimultanBearingSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[1] = btaTemp[8];
                    btaResult[2] = btaTemp[9];
                    btaResult[3] = btaTemp[10];
                    btaResult[4] = btaTemp[11];
                    btaResult[5] = btaTemp[12];
                    btaResult[6] = btaTemp[13];
                    btaResult[7] = btaTemp[14];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//


                    // пеленг от ведомой     
                    btaResult.SetAll(false);

                    intBytes = BitConverter.GetBytes(SimultanBearingSend.wBearAdd);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(intBytes);
                    result = intBytes;

                    btaTemp = new BitArray(result);

                    btaResult[0] = btaTemp[15];
                    btaResult[1] = btaTemp[0];


                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//

                    // знак, градусы долготы 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    btaResult[0] = Convert.ToBoolean(SimultanBearingSend.bSignLong);

                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//

                    // градусы долготы, минуты долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLongDegree);

                    btaTemp = new BitArray(bTemp);
                    btaResult[0] = btaTemp[7];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLongMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[2] = btaTemp[0];
                    btaResult[3] = btaTemp[1];
                    btaResult[4] = btaTemp[2];
                    btaResult[5] = btaTemp[3];
                    btaResult[6] = btaTemp[4];
                    btaResult[7] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // секунды долготы  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLongSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//


                    // знак, градусы широты
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    btaResult[0] = Convert.ToBoolean(SimultanBearingSend.bSignLat);

                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLatDegree);

                    btaTemp = new BitArray(bTemp);

                    btaResult[1] = btaTemp[0];
                    btaResult[2] = btaTemp[1];
                    btaResult[3] = btaTemp[2];
                    btaResult[4] = btaTemp[3];
                    btaResult[5] = btaTemp[4];
                    btaResult[6] = btaTemp[5];
                    btaResult[7] = btaTemp[6];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[8] = bByteBit[0];                                     //* 8 *//

                    // минуты широты     
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLatMinute);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[9] = bByteBit[0];                                     //* 9 *//

                    // секунды широты    
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(SimultanBearingSend.bLatSecond);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[10] = bByteBit[0];                                     //* 10 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Квазиодновременное пеленгование\n";

                    strInform = strInform + "Пеленг от ведущей  " + Convert.ToString(SimultanBearingSend.wBearMain) + "\n";
                    strInform = strInform + "Пеленг от ведомой  " + Convert.ToString(SimultanBearingSend.wBearAdd) + "\n";

                    strInform = strInform + Convert.ToString(SimultanBearingSend.bLongDegree) + " " + Convert.ToString(SimultanBearingSend.bLongMinute) +
                                             " " + Convert.ToString(SimultanBearingSend.bLongSecond);
                    if (SimultanBearingSend.bSignLong == 0)
                        strInform = strInform + "  Восточной долготы (код " + Convert.ToString(SimultanBearingSend.bSignLong) + ")\n";
                    else
                        strInform = strInform + "  Западной долготы (код " + Convert.ToString(SimultanBearingSend.bSignLong) + ")\n";

                    strInform = strInform + Convert.ToString(SimultanBearingSend.bLatDegree) + " " + Convert.ToString(SimultanBearingSend.bLatMinute) +
                                             " " + Convert.ToString(SimultanBearingSend.bLatSecond);
                    if (SimultanBearingSend.bSignLat == 0)
                        strInform = strInform + "  Северной широты (код " + Convert.ToString(SimultanBearingSend.bSignLat) + ")\n";
                    else
                        strInform = strInform + "  Южной широты (код " + Convert.ToString(SimultanBearingSend.bSignLat) + ")\n";

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;

                //--------------------------------                                
                case TEST_CHANNEL:

                    // шифр кодограммы
                    bBufSend[0] = Convert.ToByte(iNumCmd);                         //* 0 *//

                    // адрес отправителя 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(iAdrSender);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[1] = bByteBit[0];                                     //* 1 *//

                    // контроль канала    
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bSignDir);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[2] = bByteBit[0];                                     //* 2 *//

                    // исправность литер ведущей АСП 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[0]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[0] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[1]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[1] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[2]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[2] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[3]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[3] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[4]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[4] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[5]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[5] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterMain[6]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[6] = btaTemp[0];


                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[3] = bByteBit[0];                                     //* 3 *//

                    // исправность литер ведомой АСП
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[0]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[0] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[1]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[1] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[2]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[2] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[3]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[3] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[4]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[4] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[5]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[5] = btaTemp[0];

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bLetterAdd[6]);

                    btaTemp = new BitArray(bTemp);
                    btaResult[6] = btaTemp[0];


                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[4] = bByteBit[0];                                     //* 4 *//

                    // режим работы ведущей АСП   
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bRegimeMain);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[5] = bByteBit[0];                                     //* 5 *//

                    // режим работы ведомой АСП  
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bRegimeAdd);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[6] = bByteBit[0];                                     //* 6 *//

                    // интервал передачи 
                    btaResult.SetAll(false);

                    bTemp[0] = 0;
                    bTemp[0] = Convert.ToByte(TestChannelSend.bTime);

                    btaTemp = new BitArray(bTemp);

                    btaResult[0] = btaTemp[0];
                    btaResult[1] = btaTemp[1];
                    btaResult[2] = btaTemp[2];
                    btaResult[3] = btaTemp[3];
                    btaResult[4] = btaTemp[4];
                    btaResult[5] = btaTemp[5];
                    btaResult[6] = btaTemp[6];
                    btaResult[7] = btaTemp[7];

                    bByteBit[0] = 0;
                    btaResult.CopyTo(bByteBit, 0);

                    bBufSend[7] = bByteBit[0];                                     //* 7 *//


                    // журнал обмена      
                    strTimeCodeAdr = "";
                    strTypeCmd = "";
                    strInform = "";

                    strTimeCodeAdr = System.DateTime.Now.ToString("hh:mm:ss") + "  Шифр " + iNumCmd.ToString() + " Адрес " + iAdrSender.ToString() + "\n";
                    strTypeCmd = "Тестирование канала\n";

                    strInform = strInform + "Контроль канала  " + Convert.ToString(TestChannelSend.bSignDir) + "\n";
                    strInform = strInform + "Исправность литер ведущей СП \n";
                    strInform = strInform + "   Л1  " + Convert.ToString(TestChannelSend.bLetterMain[0]) + "\n";
                    strInform = strInform + "   Л2  " + Convert.ToString(TestChannelSend.bLetterMain[1]) + "\n";
                    strInform = strInform + "   Л3  " + Convert.ToString(TestChannelSend.bLetterMain[2]) + "\n";
                    strInform = strInform + "   Л4  " + Convert.ToString(TestChannelSend.bLetterMain[3]) + "\n";
                    strInform = strInform + "   Л5  " + Convert.ToString(TestChannelSend.bLetterMain[4]) + "\n";
                    strInform = strInform + "   Л6  " + Convert.ToString(TestChannelSend.bLetterMain[5]) + "\n";
                    strInform = strInform + "   Л7  " + Convert.ToString(TestChannelSend.bLetterMain[6]) + "\n";

                    strInform = strInform + "Исправность литер ведомой СП \n";
                    strInform = strInform + "   Л1  " + Convert.ToString(TestChannelSend.bLetterAdd[0]) + "\n";
                    strInform = strInform + "   Л2  " + Convert.ToString(TestChannelSend.bLetterAdd[1]) + "\n";
                    strInform = strInform + "   Л3  " + Convert.ToString(TestChannelSend.bLetterAdd[2]) + "\n";
                    strInform = strInform + "   Л4  " + Convert.ToString(TestChannelSend.bLetterAdd[3]) + "\n";
                    strInform = strInform + "   Л5  " + Convert.ToString(TestChannelSend.bLetterAdd[4]) + "\n";
                    strInform = strInform + "   Л6  " + Convert.ToString(TestChannelSend.bLetterAdd[5]) + "\n";
                    strInform = strInform + "   Л7  " + Convert.ToString(TestChannelSend.bLetterAdd[6]) + "\n";

                    switch (TestChannelSend.bRegimeMain)
                    {
                        case 0:
                            strInform = strInform + "Режим ведущей  Подготовка  (код " + Convert.ToString(TestChannelSend.bRegimeMain) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Режим ведущей  Радиоразведка  (код " + Convert.ToString(TestChannelSend.bRegimeMain) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Режим ведущей  Радиоподавление  (код " + Convert.ToString(TestChannelSend.bRegimeMain) + ")\n";
                            break;
                    }

                    switch (TestChannelSend.bRegimeAdd)
                    {
                        case 0:
                            strInform = strInform + "Режим ведомой  Подготовка  (код " + Convert.ToString(TestChannelSend.bRegimeAdd) + ")\n";
                            break;
                        case 1:
                            strInform = strInform + "Режим ведомой  Радиоразведка  (код " + Convert.ToString(TestChannelSend.bRegimeAdd) + ")\n";
                            break;
                        case 2:
                            strInform = strInform + "Режим ведомой  Радиоподавление  (код " + Convert.ToString(TestChannelSend.bRegimeAdd) + ")\n";
                            break;
                    }

                    SetWriteText(strTimeCodeAdr, strTypeCmd, strInform);

                    break;
            }

            try
            {
                spPortAKPP.Write(bBufSend, 0, 12);

                //if (chBytes.Checked)
                if (true)
                {
                    string strResHex = string.Empty;
                    string strResBin = string.Empty;
                    int iDecValue = 0;
                    strInform = " ";

                    for (int i = 0; i < 12; i++)
                    {
                        iDecValue = 0;
                        iDecValue = bBufSend[i];
                        strResHex = iDecValue.ToString("X2");
                        strResBin = Convert.ToString(iDecValue, 2);
                        while (strResBin.Length < 8)
                        {
                            strResBin = "0" + strResBin;
                        }
                        //rtbLogMessage.SelectionColor = Color.Indigo;
                        if (i <= 8)
                            strInform = strInform + " " + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                        else
                            strInform = strInform + Convert.ToString(i + 1) + " : " + strResHex + "    " + strResBin + "\n";
                    }

                    SetWriteByte(strInform, 12);
                }

                iRes = -1;
            }
            catch (System.Exception)
            {
                iRes = 0;
            }

            return iRes;
        }
    }
}
