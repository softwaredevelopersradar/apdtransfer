﻿using APDTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        APDTransferClass transferClass = new APDTransferClass();
        public MainWindow()
        {
            InitializeComponent();

            transferClass.COMisOpen += TransferClass_COMisOpen;
            transferClass.OnWriteByte += TransferClass_OnWriteByte;
            transferClass.OnReadByte += TransferClass_OnReadByte;


            transferClass.RequestDataEvent += TransferClass_RequestDataEvent;
            transferClass.SynchronizeEvent += TransferClass_SynchronizeEvent;
            transferClass.TextMessageEvent += TransferClass_TextMessageEvent;
            transferClass.RegimeWorkEvent += TransferClass_RegimeWorkEvent;
            transferClass.ForbidRangeFreqEvent += TransferClass_ForbidRangeFreqEvent;
            transferClass.ReconSectorRangeEvent += TransferClass_ReconSectorRangeEvent;
            transferClass.SupressSectorRangeEvent += TransferClass_SupressSectorRangeEvent;
            transferClass.SupressFWSEvent += TransferClass_SupressFWSEvent;
            transferClass.SupressFHSSEvent += TransferClass_SupressFHSSEvent;    
            transferClass.RequestBearingEvent += TransferClass_RequestBearingEvent;


            transferClass.TextMessageJEvent += TransferClass_TextMessageJEvent;
            transferClass.ReceptionEvent += TransferClass_ReceptionEvent;
            transferClass.CoordJammerEvent += TransferClass_CoordJammerEvent;
            transferClass.DataFWS1Event += TransferClass_DataFWS1Event;
            transferClass.DataFWS2Event += TransferClass_DataFWS2Event;
            transferClass.DataFHSS1Event += TransferClass_DataFHSS1Event;
            transferClass.DataFHSS2Event += TransferClass_DataFHSS2Event;
            transferClass.StateSupressFreqEvent += TransferClass_StateSupressFreqEvent;
            transferClass.ExecuteBearingEvent += TransferClass_ExecuteBearingEvent;
            transferClass.TSimultanBearingEvent += TransferClass_TSimultanBearingEvent;  
            transferClass.TestChannelEvent += TransferClass_TestChannelEvent;
        }

        private void TransferClass_COMisOpen(bool isOpen)
        {
            //Console.Beep();
        }

        private void TransferClass_OnWriteByte()
        {
            //Console.Beep();
        }

        private void TransferClass_OnReadByte()
        {
            //Console.Beep();
        }

        private void TransferClass_RequestDataEvent(APDTransferClass.TRequestData RequestDataRead)
        {
        }

        private void TransferClass_SynchronizeEvent(APDTransferClass.TSynchronize SynchronizeRead)
        {
        }

        private void TransferClass_TextMessageEvent(APDTransferClass.TTextMessage TextMessageRead)
        {
        }

        private void TransferClass_RegimeWorkEvent(APDTransferClass.TRegimeWork RegimeWorkRead)
        {
        }

        private void TransferClass_ForbidRangeFreqEvent(APDTransferClass.TForbidRangeFreq ForbidRangeFreqRead)
        {
        }

        private void TransferClass_ReconSectorRangeEvent(APDTransferClass.TReconSectorRange ReconSectorRangeRead)
        {
        }

        private void TransferClass_SupressSectorRangeEvent(APDTransferClass.TSupressSectorRange SupressSectorRangeRead)
        {
        }

        private void TransferClass_SupressFWSEvent(APDTransferClass.TSupressFWS SupressFWSRead)
        {
        }

        private void TransferClass_SupressFHSSEvent(APDTransferClass.TSupressFHSS SupressFHSSRead)
        {
        }

        private void TransferClass_RequestBearingEvent(APDTransferClass.TRequestBearing RequestBearingRead)
        {
        }

        private void TransferClass_TextMessageJEvent(APDTransferClass.TTextMessage TextMessageJRead)
        {
        }

        private void TransferClass_ReceptionEvent(APDTransferClass.TAnsReception AnsReceptionRead)
        {
        }

        private void TransferClass_CoordJammerEvent(APDTransferClass.TCoordJammer CoordJammerRead)
        {
        }

        private void TransferClass_DataFWS1Event(APDTransferClass.TDataFWSPartOne DataFWSPartOneRead)
        {
        }

        private void TransferClass_DataFWS2Event(APDTransferClass.TDataFWSPartTwo DataFWSPartTwoRead)
        {
        }

        private void TransferClass_DataFHSS1Event(APDTransferClass.TDataFHSSPartOne DataFHSSPartOneRead)
        {
        }

        private void TransferClass_DataFHSS2Event(APDTransferClass.TDataFHSSPartTwo DataFHSSPartTwoRead)
        {
        }

        private void TransferClass_StateSupressFreqEvent(APDTransferClass.TStateSupressFreq StateSupressFreqRead)
        {
        }

        private void TransferClass_ExecuteBearingEvent(APDTransferClass.TExecuteBearing ExecuteBearingRead)
        {
        }

        private void TransferClass_TSimultanBearingEvent(APDTransferClass.TSimultanBearing SimultanBearingRead)
        {
        }

        private void TransferClass_TestChannelEvent(APDTransferClass.TTestChannel TestChannelRead)
        {
        }

       
        private void Button_ClickOpenJ(object sender, RoutedEventArgs e)
        {
            //transferClass.InitRole(APDTransferClass.WhoIsWho.CommandCentre);
            transferClass.InitRole(APDTransferClass.WhoIsWho.Jammer);
            transferClass.OpenCOM(Convert.ToByte(tbJ.Text));
        }

        private void Button_ClickCloseJ(object sender, RoutedEventArgs e)
        {
            transferClass.CloseCOM();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            transferClass.SendJTextMessage(1, "Hello");
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            transferClass.SendJReceiptConfirmation(1, 1, 1, 1);
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            transferClass.SendJStationCoordinates(1, 0, 0, 29, 38, 26, 59, 36, 45);
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TDataFWSPartOne p1 = new APDTransferClass.TDataFWSPartOne();
            APDTransferClass.TDataFWSPartTwo p2 = new APDTransferClass.TDataFWSPartTwo();

            p1.bTempSource = 1;     // номер текущего ИРИ
            p1.iFreq = 1324005;           // частота           
            p1.bCodeType = 0;       // код вида            
            p1.bCodeWidth = transferClass.ConvertFromSignalWidthToWidthCode(25d);      // код ширины
            p1.bHour = 14;           // часы
            p1.bMin = 58;            // минуты
            p1.bSec = 04;            // секунды
            p1.bCodeRate = 0;       // скорость ТЛГ

            p2.bTempSource = 1;      // номер текущего ИРИ
            p2.bSignLong = 0;        // знак долготы           
            p2.bLongDegree = 56;     // градусы долготы
            p2.bLongMinute = 23;      // минуты долготы
            p2.bLongSecond = 14;      // секунды долготы
            p2.bSignLat = 0;         // знак широты
            p2.bLatDegree = 29;       // градусы широты
            p2.bLatMinute = 36;       // минуты широты
            p2.bLatSecond = 51;       // секунды широты
            p2.wBearMain = 200;        // пеленг от ведущей
            p2.wBearAdd = 30;         // пеленг от ведомой

            transferClass.SendJFRSInformation(1, p1, p2);
        }

        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TDataFHSSPartOne p1 = new APDTransferClass.TDataFHSSPartOne();
            APDTransferClass.TDataFHSSPartTwo p2 = new APDTransferClass.TDataFHSSPartTwo();

            p1.bTempSource = 1;     // номер текущего ИРИ
            p1.iFreqBegin = 562006;      // начальная частота
            p1.iFreqEnd = 622008;        // конечная частота
            p1.bHour = 14;           // часы
            p1.bMin = 58;            // минуты
            p1.bSec = 04;            // секунды
            p1.bCodeWidth = transferClass.ConvertFromFHSSWidthToWidthCode(25);      // код ширины
            p1.bCodeStep = transferClass.ConvertFromFHSSStepToStepCode(5);       // код шага

            p2.bTempSource = 1;     // номер текущего ИРИ
            transferClass.ConvertFromFHSSDurationmstoCode(15.0f, out byte bGroupDuration, out byte bDuration);
            p2.bGroupDuration = bGroupDuration;  // длительность
            p2.bDuration = bDuration;       // длительность
            p2.wBearMain = 200;       // пеленг от ведущей
            p2.wBearAdd = 30;        // пеленг от ведомой
            p2.bSignLong = 0;       // знак долготы           
            p2.bLongDegree = 55;     // градусы долготы
            p2.bLongMinute = 42;     // минуты долготы
            p2.bLongSecond = 28;     // секунды долготы
            p2.bSignLat = 0;        // знак широты
            p2.bLatDegree = 28;      // градусы широты
            p2.bLatMinute = 06;      // минуты широты
            p2.bLatSecond = 13;      // секунды широты   
            p2.bCodeType = 0;       // код вида модуляции     


            transferClass.SendJFHSSInformation(1, p1, p2);
        }

        private void Button_Click6(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TStateSupressFreq p = new APDTransferClass.TStateSupressFreq();
            p.Init();

            //p.bStateWork;   // инф-ция о работе    
            //p.bStateLongTerm;  // инф-ция о долговременности           
            //p.bStateSupress;   // инф-ция о подвлении           
            p.bTempSubRange = 1;   // номер поддиапазона
            p.bSighF = 0;          // признак ФЧ/ППРЧ
            p.bStateFHSS = 0;      // инф-ция ППРЧ

            transferClass.SendJReceiptJammingFrequencies(1, p);
        }

        private void Button_Click7(object sender, RoutedEventArgs e)
        {
            transferClass.SendJExBearingAnswer(1, 359);
        }

        private void Button_Click8(object sender, RoutedEventArgs e)
        {
            transferClass.SendJQuasiBearingAnswer(1, 200, 30, 0, 0, 28, 33, 21, 59, 15, 52);
        }

        private void Button_Click9(object sender, RoutedEventArgs e)
        {
            transferClass.SendJChanelTesting(1, 1, 0, 0, new byte[7], new byte[7]);
        }

        private void Button_ClickOpenPU(object sender, RoutedEventArgs e)
        {
            transferClass.InitRole(APDTransferClass.WhoIsWho.CommandCentre);
            transferClass.OpenCOM(Convert.ToByte(tbPU.Text));
        }

        private void Button_ClickClosePU(object sender, RoutedEventArgs e)
        {
            transferClass.CloseCOM();
        }

        private void Button_Click10(object sender, RoutedEventArgs e)
        {
            transferClass.SendССDataRequest(0, APDTransferClass.TypeComand.Квитанция);
        }

        private void Button_Click11(object sender, RoutedEventArgs e)
        {
            transferClass.SendССSync(0, DateTime.Now);
        }

        private void Button_Click12(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCTextMessage(0, "Helloooo");
        }

        private void Button_Click13(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCWorkMode(0, APDTransferClass.Mode.Подготовка);
        }

        private void Button_Click14(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCForbiddenRanges(0, APDTransferClass.Action.Добавить, 1, 1550000, 1575000);
        }

        private void Button_Click15(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCSectorsAndRangesRI(0, APDTransferClass.Action.Добавить, 1, 1452003, 1975001, 45, 95);
        }

        private void Button_Click16(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCSectorsAndRangesRS(0, APDTransferClass.Action.Добавить, 1, 1, 1452003, 1975001, 45, 95);
        }

        private void Button_Click17(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TSupressFWS p = new APDTransferClass.TSupressFWS();

            p.bAction = (byte)APDTransferClass.Action.Добавить;    // действие
            p.bTempRow = 1;            // номер строки            
            p.iFreq = 1579004;               // частота
            p.iCodeHind = 3;           // код помехи
            p.bPrior = 1;              // приоритет
            p.bCodeModulation = (byte)APDTransferClass.TypeRS.ЧМ;     // код вида
            p.bCodeDeviation = (byte)APDTransferClass.Deviation1.dev1_75;      // код ширины
            p.bCodeManipulation = 0;   // код скорости

            transferClass.SendCCFRSonRS(0, p);
        }

        private void Button_Click18(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TSupressFHSS p = new APDTransferClass.TSupressFHSS();

            p.bAction = (byte)APDTransferClass.Action.Добавить;         // действие
            p.bTempRow = 1;            // номер строки            
            p.iFreqBegin = 1605003;          // начальная частота
            p.iFreqEnd = 1718002;            // конечная частота 

            transferClass.ConvertFromFHSSDurationmstoCode(15.0f, out byte bGroupDuration, out byte bDuration);
            p.bGroupDuration = bGroupDuration;      // длительность
            p.bDuration = bDuration;           // длительность            

            p.bCodeStep = transferClass.ConvertFromFHSSStepToStepCode(5);       // код шага;           // код шага

            p.bCodeModulation = (byte)APDTransferClass.TypeRS.ЧМ; ;     // код вида
            p.bCodeDeviation = (byte)APDTransferClass.Deviation1.dev1_75;     // код ширины
            p.bCodeManipulation = 0;   // код ширины

            transferClass.SendCCFHSSonRS(0, p);
        }

        private void Button_Click19(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCExOrQuasiBearingRequest(0, APDTransferClass.TypeReqBearing.Исполнительное, 1590000, 15);
        }
    }
}
